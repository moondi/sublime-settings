{
  "name": "Monokai Extended",
  "author": "github.com/jonschlinkert",
  "variables":
  {
    "black": "#000000",
    "black2": "#1c1c1c",
    "black3": "#222222",
    "blue": "#678cb1",
    "blue2": "#3bc0f0",
    "cyan": "#66d9ef",
    "grey": "#c8cecc",
    "grey2": "#3b3a32",
    "grey3": "#333333",
    "grey4": "#444444",
    "grey5": "#565656",
    "orange": "#fd971f",
    "orange2": "#df9400",
    "orange3": "#f6aa11",
    "orange4": "#ff9117",
    "orange5": "#ffe792",
    "purple": "#be84ff",
    "purple2": "#ae81ff",
    "purple3": "#967efb",
    "red": "#ff0000",
    "red2": "#d3201f",
    "red3": "#ec3533",
    "red4": "#dc322f",
    "red5": "#ff4a52",
    "red6": "#ff3a28",
    "red7": "#b42a1d",
    "red8": "#d02000",
    "red9": "#f92672",
    "red10": "#e42e70",
    "white": "#ffffff",
    "white2": "#f8f8f0",
    "white3": "#f8f8f2",
    "white4": "#e0fdce",
    "white5": "#e0eddd",
    "yellow": "#e6db74",
    "yellow2": "#ddb700",
    "yellow3": "#ffffaa",
    "yellow4": "#635f2d",
    "yellow5": "#a6e22e",
    "yellow6": "#7fb11b",
    "yellow7": "#cfcfc2",
    "yellow8": "#636050",
    "yellow9": "#75715e",
    "yellow10": "#7c7865"
  },
  "globals":
  {
    "foreground": "var(white3)",
    "background": "var(black3)",
    "caret": "var(white2)",
    "invisibles": "var(grey2)",
    "line_highlight": "var(grey3)",
    "selection": "var(grey4)",
    "selection_border": "var(black2)",
    "active_guide": "var(yellow6)",
    "stack_guide": "var(yellow4)",
    "find_highlight_foreground": "var(black)",
    "find_highlight": "var(orange5)",
    "brackets_options": "underline",
    "brackets_foreground": "color(var(white3) alpha(0.65))",
    "bracket_contents_options": "underline",
    "bracket_contents_foreground": "color(var(white3) alpha(0.65))",
    "tags_options": "stippled_underline"
  },
  "rules":
  [
    {
      "name": "Comment",
      "scope": "comment",
      "foreground": "var(yellow9)"
    },
    {
      "name": "String",
      "scope": "string",
      "foreground": "var(yellow)"
    },
    {
      "name": "Number",
      "scope": "constant.numeric",
      "foreground": "var(purple)"
    },
    {
      "name": "Constant: Built-in",
      "scope": "constant.language, meta.preprocessor",
      "foreground": "var(purple)"
    },
    {
      "name": "Constant: User-defined",
      "scope": "constant.character, constant.other",
      "foreground": "var(purple)"
    },
    {
      "name": "Variable",
      "scope": "variable.language, variable.other",
      "foreground": "var(blue)"
    },
    {
      "name": "Keyword",
      "scope": "keyword",
      "foreground": "var(red9)"
    },
    {
      "name": "Storage",
      "scope": "storage",
      "foreground": "var(red9)"
    },
    {
      "name": "Storage type",
      "scope": "storage.type",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "Class name",
      "scope": "entity.name.class",
      "foreground": "var(yellow5)",
      "font_style": "underline"
    },
    {
      "name": "Inherited class",
      "scope": "entity.other.inherited-class",
      "foreground": "var(yellow5)",
      "font_style": "italic underline"
    },
    {
      "name": "Function name",
      "scope": "entity.name.function",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Function argument",
      "scope": "variable.parameter",
      "foreground": "var(orange)",
      "font_style": "italic"
    },
    {
      "name": "Tag name",
      "scope": "entity.name.tag",
      "foreground": "var(red9)"
    },
    {
      "name": "Tag attribute",
      "scope": "entity.other.attribute-name",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Library function",
      "scope": "support.function",
      "foreground": "var(cyan)"
    },
    {
      "name": "Library constant",
      "scope": "support.constant",
      "foreground": "var(cyan)"
    },
    {
      "name": "Library class/type",
      "scope": "support.type, support.class",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "Library variable",
      "scope": "support.other.variable"
    },
    {
      "name": "String constant",
      "scope": "string constant",
      "foreground": "var(cyan)"
    },
    {
      "name": "String.regexp",
      "scope": "string.regexp",
      "foreground": "var(orange3)"
    },
    {
      "name": "String variable",
      "scope": "string variable",
      "foreground": "var(white)"
    },
    {
      "name": "Variable: punctuation",
      "scope": "punctuation.definition.variable",
      "foreground": "var(white)"
    },
    {
      "name": "Entity",
      "scope": "entity",
      "foreground": "var(yellow5)"
    },
    {
      "name": "HTML: Doctype/XML Processing",
      "scope": "meta.tag.sgml.doctype.xml, declaration.sgml.html declaration.doctype, declaration.sgml.html declaration.doctype entity, declaration.sgml.html declaration.doctype string,  declaration.xml-processing, declaration.xml-processing entity, declaration.xml-processing string, doctype",
      "foreground": "var(grey)"
    },
    {
      "name": "HTML: Comment Block",
      "scope": "comment.block.html",
      "foreground": "var(yellow10)"
    },
    {
      "name": "HTML: Script",
      "scope": "entity.name.tag.script.html",
      "font_style": "italic"
    },
    {
      "name": "HTML: Attribute punctuation",
      "scope": "text.html.basic meta.tag.other.html, text.html.basic meta.tag.any.html, text.html.basic meta.tag.block.any, text.html.basic meta.tag.inline.any, text.html.basic meta.tag.structure.any.html,  text.html.basic source.js.embedded.html, punctuation.separator.key-value.html",
      "foreground": "var(yellow5)"
    },
    {
      "name": "HTML: Attributes",
      "scope": "text.html.basic entity.other.attribute-name.html",
      "foreground": "var(yellow5)"
    },
    {
      "name": "HTML: Quotation Marks",
      "scope": "text.html.basic meta.tag.structure.any.html punctuation.definition.string.begin.html, punctuation.definition.string.begin.html, punctuation.definition.string.end.html ",
      "foreground": "var(white)"
    },
    {
      "name": "HTML: Tags punctuation",
      "scope": "punctuation.definition.tag.end, punctuation.definition.tag.begin, punctuation.definition.tag",
      "foreground": "var(white)"
    },
    {
      "name": "Handlebars: Variable",
      "scope": "variable.parameter.handlebars",
      "foreground": "var(orange3)"
    },
    {
      "name": "Handlebars: Constant",
      "scope": "support.constant.handlebars, meta.function.block.start.handlebars",
      "foreground": "var(cyan)"
    },
    {
      "name": "CSS: @at-rule",
      "scope": "meta.preprocessor.at-rule keyword.control.at-rule",
      "foreground": "var(orange3)"
    },
    {
      "name": "CSS: #Id",
      "scope": "meta.selector.css entity.other.attribute-name.id",
      "foreground": "var(orange3)"
    },
    {
      "name": "CSS: #Id for SCSS",
      "scope": "entity.other.attribute-name.id",
      "foreground": "var(orange3)"
    },
    {
      "name": "CSS: .class",
      "scope": "meta.selector.css entity.other.attribute-name.class",
      "foreground": "var(yellow5)"
    },
    {
      "name": "CSS: Property Name",
      "scope": "support.type.property-name.css",
      "foreground": "var(cyan)"
    },
    {
      "name": "CSS: Constructor Argument",
      "scope": "meta.constructor.argument.css",
      "foreground": "var(orange3)"
    },
    {
      "name": "CSS: {}",
      "scope": "punctuation.section.property-list.css",
      "foreground": "var(white)"
    },
    {
      "name": "CSS: Tag Punctuation",
      "scope": "punctuation.definition.tag.css",
      "foreground": "var(red9)"
    },
    {
      "name": "CSS: : ,",
      "scope": "punctuation.separator.key-value.css, punctuation.terminator.rule.css",
      "foreground": "var(white)"
    },
    {
      "name": "CSS :pseudo",
      "scope": "entity.other.attribute-name.pseudo-element.css, entity.other.attribute-name.pseudo-class.css, entity.other.attribute-name.pseudo-selector.css",
      "foreground": "var(yellow5)"
    },
    {
      "name": "LESS variables",
      "scope": "variable.other.less",
      "foreground": "var(white)"
    },
    {
      "name": "LESS mixins",
      "scope": "entity.other.less.mixin",
      "foreground": "var(white4)",
      "font_style": "italic"
    },
    {
      "name": "LESS: Extend",
      "scope": "entity.other.attribute-name.pseudo-element.less",
      "foreground": "var(orange4)"
    },
    {
      "name": "JS: Function Name",
      "scope": "meta.function.js, entity.name.function.js, support.function.dom.js",
      "foreground": "var(yellow5)"
    },
    {
      "name": "JS: Storage Type",
      "scope": "storage.type.js",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "JS: Source",
      "scope": "text.html.basic source.js.embedded.html",
      "foreground": "var(white)"
    },
    {
      "name": "JS: Function",
      "scope": "storage.type.function.js",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "JS: Numeric Constant",
      "scope": "constant.numeric.js",
      "foreground": "var(purple2)"
    },
    {
      "name": "JS: []",
      "scope": "meta.brace.square.js",
      "foreground": "var(white)"
    },
    {
      "name": "JS: ()",
      "scope": "meta.brace.round, punctuation.definition.parameters.begin.js, punctuation.definition.parameters.end.js",
      "foreground": "var(white)"
    },
    {
      "name": "JS: {}",
      "scope": "meta.brace.curly.js",
      "foreground": "var(white)"
    },
    {
      "name": "JSON String",
      "scope": "meta.structure.dictionary.json string.quoted.double.json",
      "foreground": "var(yellow7)"
    },
    {
      "name": "PHP: []",
      "scope": "keyword.operator.index-start.php, keyword.operator.index-end.php",
      "foreground": "var(white)"
    },
    {
      "name": "PHP: Array",
      "scope": "meta.array.php",
      "foreground": "var(white)"
    },
    {
      "name": "PHP: Array()",
      "scope": "meta.array.php support.function.construct.php, meta.array.empty.php support.function.construct.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Array Construct",
      "scope": "support.function.construct.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Storage Type Function",
      "scope": "storage.type.function.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Numeric Constant",
      "scope": "constant.numeric.php",
      "foreground": "var(purple)"
    },
    {
      "name": "PHP: New",
      "scope": "keyword.other.new.php",
      "foreground": "var(red10)"
    },
    {
      "name": "PHP: ::",
      "scope": "support.class.php",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "PHP: Other Property",
      "scope": "variable.other.property.php",
      "foreground": "var(orange3)"
    },
    {
      "name": "PHP: Class",
      "scope": "storage.modifier.extends.php, storage.type.class.php, keyword.operator.class.php",
      "foreground": "var(red10)"
    },
    {
      "name": "PHP: Inherited Class",
      "scope": "meta.other.inherited-class.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Storage Type",
      "scope": "storage.type.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Function",
      "scope": "entity.name.function.php",
      "foreground": "var(yellow5)"
    },
    {
      "name": "PHP: Function Construct",
      "scope": "support.function.construct.php",
      "foreground": "var(cyan)"
    },
    {
      "name": "PHP: Function Call",
      "scope": "entity.name.type.class.php, meta.function-call.php, meta.function-call.static.php, meta.function-call.object.php",
      "foreground": "var(white)"
    },
    {
      "name": "PHP: Comment",
      "scope": "keyword.other.phpdoc",
      "foreground": "var(yellow10)"
    },
    {
      "name": "PHP: Source Emebedded",
      "scope": "source.php.embedded.block.html",
      "foreground": "var(white)"
    },
    {
      "name": "Invalid",
      "scope": "invalid",
      "foreground": "var(white2)",
      "background": "var(red9)"
    },
    {
      "name": "Invalid deprecated",
      "scope": "invalid.deprecated",
      "foreground": "var(white2)",
      "background": "var(purple2)"
    },
    {
      "name": "diff.header",
      "scope": "meta.diff, meta.diff.header",
      "foreground": "var(yellow9)"
    },
    {
      "name": "diff.deleted",
      "scope": "markup.deleted",
      "foreground": "var(red9)"
    },
    {
      "name": "diff.inserted",
      "scope": "markup.inserted",
      "foreground": "var(yellow5)"
    },
    {
      "name": "diff.changed",
      "scope": "markup.changed",
      "foreground": "var(yellow)"
    },
    {
      "name": "diff.range",
      "scope": "meta.diff, meta.diff.range",
      "foreground": "var(blue2)"
    },
    {
      "name": "Python: storage",
      "scope": "storage.type.class.python, storage.type.function.python, storage.modifier.global.python",
      "foreground": "var(blue2)"
    },
    {
      "name": "Python: import",
      "scope": "keyword.control.import.python, keyword.control.import.from.python",
      "foreground": "color(var(red9) alpha(0.87))"
    },
    {
      "name": "Python: Support.exception",
      "scope": "support.type.exception.python",
      "foreground": "var(cyan)"
    },
    {
      "name": "Perl: variables",
      "scope": "punctuation.definition.variable.perl, variable.other.readwrite.global.perl, variable.other.predefined.perl, keyword.operator.comparison.perl",
      "foreground": "var(red10)"
    },
    {
      "name": "Perl: functions",
      "scope": "support.function.perl",
      "foreground": "var(cyan)"
    },
    {
      "name": "Perl: comments",
      "scope": "comment.line.number-sign.perl",
      "foreground": "var(yellow9)",
      "font_style": "italic"
    },
    {
      "name": "Perl: quotes",
      "scope": "punctuation.definition.string.begin.perl, punctuation.definition.string.end.perl",
      "foreground": "var(white)"
    },
    {
      "name": "Perl: char",
      "scope": "constant.character.escape.perl",
      "foreground": "var(red4)"
    },
    {
      "name": "Ruby: Constant",
      "scope": "constant.language.ruby, constant.numeric.ruby",
      "foreground": "var(purple2)"
    },
    {
      "name": "Ruby: Variable definition",
      "scope": "punctuation.definition.variable.ruby",
      "foreground": "var(blue)"
    },
    {
      "name": "Ruby: Function Name",
      "scope": "meta.function.method.with-arguments.ruby",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Ruby: Variable",
      "scope": "variable.language.ruby",
      "foreground": "var(white)"
    },
    {
      "name": "Ruby: Function",
      "scope": "entity.name.function.ruby",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Ruby: Keyword Control",
      "scope": "keyword.control.ruby, keyword.control.def.ruby",
      "foreground": "var(red9)",
      "font_style": "bold"
    },
    {
      "name": "Ruby: Class",
      "scope": "keyword.control.class.ruby, meta.class.ruby",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Ruby: Class Name",
      "scope": "entity.name.type.class.ruby",
      "foreground": "var(cyan)"
    },
    {
      "name": "Ruby: Keyword",
      "scope": "keyword.control.ruby",
      "foreground": "var(red9)"
    },
    {
      "name": "Ruby: Support Class",
      "scope": "support.class.ruby",
      "foreground": "var(cyan)"
    },
    {
      "name": "Ruby: Special Method",
      "scope": "keyword.other.special-method.ruby",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Ruby: Constant Other",
      "scope": "variable.other.constant.ruby",
      "foreground": "var(cyan)"
    },
    {
      "name": "Ruby: :symbol",
      "scope": "constant.other.symbol.ruby",
      "foreground": "var(purple2)"
    },
    {
      "name": "Ruby: Punctuation Section",
      "scope": "punctuation.section.embedded.ruby, punctuation.definition.string.begin.ruby, punctuation.definition.string.end.ruby",
      "foreground": "var(red9)"
    },
    {
      "name": "Ruby: Special Method",
      "scope": "keyword.other.special-method.ruby",
      "foreground": "var(red10)"
    },
    {
      "name": "Markdown: plain",
      "scope": "text.html.markdown",
      "foreground": "var(white)"
    },
    {
      "name": "Markup: raw inline",
      "scope": "text.html.markdown markup.raw.inline",
      "foreground": "var(red3)"
    },
    {
      "name": "Markdown: linebreak",
      "scope": "text.html.markdown meta.dummy.line-break",
      "foreground": "var(white5)"
    },
    {
      "name": "Markdown: heading",
      "scope": "markdown.heading, markup.heading | markup.heading entity.name, markup.heading.markdown punctuation.definition.heading.markdown",
      "foreground": "var(orange)"
    },
    {
      "name": "Markup: italic",
      "scope": "markup.italic",
      "foreground": "var(red10)",
      "font_style": "italic"
    },
    {
      "name": "Markup: bold",
      "scope": "markup.bold",
      "foreground": "var(red9)",
      "font_style": "bold"
    },
    {
      "name": "Markup: underline",
      "scope": "markup.underline",
      "foreground": "var(yellow5)",
      "font_style": "underline"
    },
    {
      "name": "Markdown: Blockquote",
      "scope": "markup.quote, punctuation.definition.blockquote.markdown",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "Markup: Quote",
      "scope": "markup.quote",
      "foreground": "var(cyan)",
      "font_style": "italic"
    },
    {
      "name": "Markdown: Link",
      "scope": "string.other.link.title.markdown",
      "foreground": "var(cyan)",
      "font_style": "underline"
    },
    {
      "name": "Markup: Raw block",
      "scope": "markup.raw.block",
      "foreground": "var(purple2)"
    },
    {
      "name": "Markdown: List Items Punctuation",
      "scope": "punctuation.definition.list_item.markdown",
      "foreground": "var(white)"
    },
    {
      "name": "Markdown: Raw Block fenced",
      "scope": "markup.raw.block.fenced.markdown",
      "foreground": "var(white)",
      "background": "var(black3)"
    },
    {
      "name": "Markdown: Fenced Bode Block",
      "scope": "punctuation.definition.fenced.markdown, variable.language.fenced.markdown",
      "foreground": "var(yellow8)",
      "background": "var(black3)"
    },
    {
      "name": "Markdown: Fenced Language",
      "scope": "variable.language.fenced.markdown",
      "foreground": "var(yellow10)"
    },
    {
      "name": "Markdown: Separator",
      "scope": "meta.separator",
      "foreground": "color(var(white) alpha(0.2))",
      "background": "color(var(white) alpha(0.059))",
      "font_style": "bold"
    },
    {
      "name": "Markup: table",
      "scope": "markup.table",
      "foreground": "var(red7)",
      "background": "color(var(red6) alpha(0.1))"
    },
    {
      "name": "Other: Removal",
      "scope": "other.package.exclude, other.remove",
      "foreground": "var(red2)"
    },
    {
      "name": "Shell: builtin",
      "scope": "support.function.builtin.shell",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Shell: variable",
      "scope": "variable.other.normal.shell",
      "foreground": "var(cyan)"
    },
    {
      "name": "Shell: DOTFILES",
      "scope": "source.shell",
      "foreground": "var(white)"
    },
    {
      "name": "Shell: meta scope in loop",
      "scope": "meta.scope.for-in-loop.shell, variable.other.loop.shell",
      "foreground": "var(orange)"
    },
    {
      "name": "Shell: Function name",
      "scope": "entity.name.function.shell",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Shell: Quotation Marks",
      "scope": "punctuation.definition.string.end.shell, punctuation.definition.string.begin.shell",
      "foreground": "var(white)"
    },
    {
      "name": "Shell: Meta Block",
      "scope": "meta.scope.case-block.shell, meta.scope.case-body.shell",
      "foreground": "var(orange)"
    },
    {
      "name": "Shell: []",
      "scope": "punctuation.definition.logical-expression.shell",
      "foreground": "var(white)"
    },
    {
      "name": "Shell: Comment",
      "scope": "comment.line.number-sign.shell",
      "foreground": "var(yellow10)",
      "font_style": "italic"
    },
    {
      "name": "Makefile: Comment",
      "scope": "comment.line.number-sign.makefile",
      "foreground": "var(yellow10)"
    },
    {
      "name": "Makefile: Comment punctuation",
      "scope": "punctuation.definition.comment.makefile",
      "foreground": "var(yellow10)"
    },
    {
      "name": "Makefile: Variables",
      "scope": "variable.other.makefile",
      "foreground": "var(red9)"
    },
    {
      "name": "Makefile: Function name",
      "scope": "entity.name.function.makefile",
      "foreground": "var(yellow5)"
    },
    {
      "name": "Makefile: Function",
      "scope": "meta.function.makefile",
      "foreground": "var(cyan)"
    },
    {
      "name": "GitGutter deleted",
      "scope": "markup.deleted.git_gutter",
      "foreground": "var(red9)"
    },
    {
      "name": "GitGutter inserted",
      "scope": "markup.inserted.git_gutter",
      "foreground": "var(yellow5)"
    },
    {
      "name": "GitGutter changed",
      "scope": "markup.changed.git_gutter",
      "foreground": "var(purple3)"
    },
    {
      "name": "GitGutter ignored",
      "scope": "markup.ignored.git_gutter",
      "foreground": "var(grey5)"
    },
    {
      "name": "GitGutter untracked",
      "scope": "markup.untracked.git_gutter",
      "foreground": "var(grey5)"
    },
    {
      "name": "SublimeLinter Annotations",
      "scope": "sublimelinter.annotations",
      "foreground": "var(grey3)",
      "background": "var(yellow3)"
    },
    {
      "name": "SublimeLinter Error Outline",
      "scope": "sublimelinter.outline.illegal",
      "foreground": "var(white)",
      "background": "var(red5)"
    },
    {
      "name": "SublimeLinter Error Underline",
      "scope": "sublimelinter.underline.illegal",
      "background": "var(red)"
    },
    {
      "name": "SublimeLinter Warning Outline",
      "scope": "sublimelinter.outline.warning",
      "foreground": "var(white)",
      "background": "var(orange2)"
    },
    {
      "name": "SublimeLinter Warning Underline",
      "scope": "sublimelinter.underline.warning",
      "background": "var(red)"
    },
    {
      "name": "SublimeLinter Violation Outline",
      "scope": "sublimelinter.outline.violation",
      "foreground": "var(white)",
      "background": "color(var(white) alpha(0.2))"
    },
    {
      "name": "SublimeLinter Violation Underline",
      "scope": "sublimelinter.underline.violation",
      "background": "var(red)"
    },
    {
      "scope": "constant.numeric.line-number.find-in-files - match",
      "foreground": "color(var(purple2) alpha(0.63))"
    },
    {
      "scope": "entity.name.filename.find-in-files",
      "foreground": "var(yellow)"
    },
    {
      "name": "SublimeLinter Error",
      "scope": "sublimelinter.mark.error",
      "foreground": "var(red8)"
    },
    {
      "name": "SublimeLinter Warning",
      "scope": "sublimelinter.mark.warning",
      "foreground": "var(yellow2)"
    },
    {
      "name": "SublimeLinter Gutter Mark",
      "scope": "sublimelinter.gutter-mark",
      "foreground": "var(white)"
    }
  ]
}
