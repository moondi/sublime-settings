# 1 fold - good
Something1 = ({ prop1 = 'default', prop2 }) ->
  <div className='text-center'>
    {prop1}
  </div>

# COMPILED OUTPUT:
# Something1 = function({prop1 = 'default', prop2}) {
#   return <div className='text-center'>
#     {prop1}
#   </div>;
# };

# 2 folds - not ideal
# - but is the most readable and maintable (minimize churn) when destructuring many props (ie. > 4)
# - compiled output is the same as Something1
Something2 = ({
  prop1 = 'default'
  prop2
}) ->
  <div className='text-center'>
    {prop1}
  </div>

# 1 fold - good, but it doesn't compile as 'cleanly'
Something3_DONT_USE = (props) ->
  {
    prop1 = 'default'
    prop2
  } = props

  <div className='text-center'>
    {prop1}
  </div>

# COMPILED OUTPUT:
# Something3_DONT_USE = function(props) {
#   var prop1, prop2;
#   ({prop1 = 'default', prop2} = props);
#   return <div className='text-center'>
#     {prop1}
#   </div>;
# };

# well... this looks like it's the winner
# - 1 fold
# - compiled output is the same as 1 and 2
# - bit janky-looking, but behaves as desired, is readable, and compiles down to the output we want
#   - coffeelint doesn't like the lack of indentation after the ->
#   - vim (Daniel) doesn't like the extra indentation to appease coffeelint
Something4 = (
  {
    prop1 = 'default'
    prop2
  }) ->
  <div className='text-center'>
    {prop1}
  </div>

# this is what we decided on as team
BestSomething =
  ({
    prop1 = 'default'
    prop2
  }) ->
    <div className='text-center'>
      {prop1}
    </div>
