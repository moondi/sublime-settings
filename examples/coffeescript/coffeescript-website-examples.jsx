var A, Base, Robot;

Hive.Bee = class Bee extends Hive {
  constructor(name) {
    super(name);
  }

};

// mild metaprogramming - https://github.com/jashkenas/coffeescript/blob/master/test/classes.coffee
Base = class Base {
  static attr(name) {
    return this.prototype[name] = function(val) {
      if (arguments.length > 0) {
        return this[`_${name}`] = val;
      } else {
        return this[`_${name}`];
      }
    };
  }

};

Robot = (function() {
  class Robot extends Base {};

  Robot.attr('power');

  Robot.attr('speed');

  return Robot;

}).call(this);

A = (function() {
  class A {};

  A.B = class {
    static c() {
      return 5;
    }

  };

  return A;

}).call(this);

A.B.c() === 5;

// ========
// Overview
// ========

// Assignment:
var cubes, list, math, num, number, opposite, race, square;

number = 42;

opposite = true;

if (opposite) {
  // Conditions:
  number = -42;
}

// Functions:
square = function(x) {
  return x * x;
};

// Arrays:
list = [1, 2, 3, 4, 5];

// Objects:
math = {
  root: Math.sqrt,
  square: square,
  cube: function(x) {
    return x * square(x);
  }
};

// Splats:
race = function(winner, ...runners) {
  return print(winner, runners);
};

if (typeof elvis !== "undefined" && elvis !== null) {
  // Existence:
  alert("I knew it!");
}

// Array comprehensions:
cubes = (function() {
  var i, len, results;
  results = [];
  for (i = 0, len = list.length; i < len; i++) {
    num = list[i];
    results.push(math.cube(num));
  }
  return results;
})();



// =========
// Functions
// =========

var cube, square;

square = function(x) {
  return x * x;
};

cube = function(x) {
  return square(x) * x;
};

// default arguments
var fill;

fill = function(container, liquid = "coffee") {
  return `Filling the ${container} with ${liquid}...`;
};



// =======
// Strings
// =======

var author, quote, sentence;

author = "Wittgenstein";

quote = `A picture is a fact. -- ${author}`;

sentence = `${22 / 7} is a decent approximation of π`;

// multiline
var mobyDick;

mobyDick = "Call me Ishmael. Some years ago -- never mind how long precisely -- having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world...";

// block
var html;

html = `<strong>
  cup of coffeescript
</strong>`;



// ==================
// Objects and Arrays
// ==================

var bitlist, kids, singers, song;

song = ["do", "re", "mi", "fa", "so"];

singers = {
  Jagger: "Rock",
  Elvis: "Roll"
};

bitlist = [1, 0, 1, 0, 0, 1, 1, 1, 0];

kids = {
  brother: {
    name: "Max",
    age: 11
  },
  sister: {
    name: "Ida",
    age: 9
  }
};

// variable-as-key-shorthand
var mask, name, output, turtle, weapon;

name = "Michelangelo";

mask = "orange";

weapon = "nunchuks";

turtle = {name, mask, weapon};

output = `${turtle.name} wears an ${turtle.mask} mask. Watch out for his ${turtle.weapon}!`;



// ========
// Comments
// ========

/*
Fortune Cookie Reader v1.0
Released under the MIT License
*/
var sayFortune;

sayFortune = function(fortune) {
  return console.log(fortune); // in bed!
};



// ===================================
// Lexical Scoping and Variable Safety
// ===================================

var changeNumbers, inner, outer;

outer = 1;

changeNumbers = function() {
  var inner;
  inner = -1;
  return outer = 10;
};

inner = changeNumbers();



// ===========================================
// If, Else, Unless and Conditional Assignment
// ===========================================

var date, mood;

if (singing) {
  mood = greatlyImproved;
}

if (happy && knowsIt) {
  clapsHands();
  chaChaCha();
} else {
  showIt();
}

date = friday ? sue : jill;



// ========================================
// Splats, or Rest Parameters/Spread Syntax
// ========================================

var awardMedals, contenders, gold, rest, silver;

gold = silver = rest = "unknown";

awardMedals = function(first, second, ...others) {
  gold = first;
  silver = second;
  return rest = others;
};

contenders = ["Michael Phelps", "Liu Xiang", "Yao Ming", "Allyson Felix", "Shawn Johnson", "Roman Sebrle", "Guo Jingjing", "Tyson Gay", "Asafa Powell", "Usain Bolt"];

awardMedals(...contenders);

alert(`Gold: ${gold}
Silver: ${silver}
The Field: ${rest.join(', ')}`);

// elide (merge) array elements
var all, popular, unwanted;

popular = ['pepperoni', 'sausage', 'cheese'];

unwanted = ['anchovies', 'olives'];

all = [...popular, ...unwanted, 'mushrooms'];

// and object properties
var currentUser, user;

user = {
  name: 'Werner Heisenberg',
  occupation: 'theoretical physicist'
};

currentUser = {
  ...user,
  status: 'Uncertain'
};



// ========================
// Loops and Comprehensions
// ========================

// Eat lunch.
var courses, dish, eat, food, foods, i, j, k, l, len, len1, len2, menu, ref;

eat = function(food) {
  return `${food} eaten.`;
};

ref = ['toast', 'cheese', 'wine'];
for (j = 0, len = ref.length; j < len; j++) {
  food = ref[j];
  eat(food);
}

// Fine five course dining.
courses = ['greens', 'caviar', 'truffles', 'roast', 'cake'];

menu = function(i, dish) {
  return `Menu Item ${i}: ${dish}`;
};

for (i = k = 0, len1 = courses.length; k < len1; i = ++k) {
  dish = courses[i];
  menu(i + 1, dish);
}

// Health conscious meal.
foods = ['broccoli', 'spinach', 'chocolate'];

for (l = 0, len2 = foods.length; l < len2; l++) {
  food = foods[l];
  if (food !== 'chocolate') {
    eat(food);
  }
}

// loops
var countdown, num;

countdown = (function() {
  var i, results;
  results = [];
  for (num = i = 10; i >= 1; num = --i) {
    results.push(num);
  }
  return results;
})();

// iterating objects
var age, ages, child, yearsOld;

yearsOld = {
  max: 10,
  ida: 9,
  tim: 11
};

ages = (function() {
  var results;
  results = [];
  for (child in yearsOld) {
    age = yearsOld[child];
    results.push(`${child} is ${age}`);
  }
  return results;
})();

// while
// Econ 101
var lyrics, num;

if (this.studyingEconomics) {
  while (supply > demand) {
    buy();
  }
  while (!(supply > demand)) {
    sell();
  }
}

// Nursery Rhyme
num = 6;

lyrics = (function() {
  var results;
  results = [];
  while (num -= 1) {
    results.push(`${num} little monkeys, jumping on the bed. One fell out and bumped his head.`);
  }
  return results;
})();

// closures
var filename, i, len;

for (i = 0, len = list.length; i < len; i++) {
  filename = list[i];
  (function(filename) {
    if (filename !== '.DS_Store' && filename !== 'Thumbs.db' && filename !== 'ehthumbs.db') {
      return fs.readFile(filename, function(err, contents) {
        return compile(filename, contents.toString());
      });
    }
  })(filename);
}



// ======================================
// Array Slicing and Splicing with Ranges
// ======================================

var copy, end, middle, numbers, start;

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

start = numbers.slice(0, 3);

middle = numbers.slice(3, -2);

end = numbers.slice(-2);

copy = numbers.slice(0);

// splicing
var numbers, ref,
  splice = [].splice;

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

splice.apply(numbers, [3, 4].concat(ref = [-3, -4, -5, -6])), ref;



// ===========================================================
// Everything is an Expression (at least, as much as possible)
// ===========================================================

var eldest, grade;

grade = function(student) {
  if (student.excellentWork) {
    return "A+";
  } else if (student.okayStuff) {
    if (student.triedHard) {
      return "B";
    } else {
      return "B-";
    }
  } else {
    return "C";
  }
};

eldest = 24 > 21 ? "Liz" : "Ike";

// assignment within expressions for new variables
var one, six, three, two;

six = (one = 1) + (two = 2) + (three = 3);

// statements as expressions
// The first ten global properties.
var globals, name;

globals = ((function() {
  var results;
  results = [];
  for (name in window) {
    results.push(name);
  }
  return results;
})()).slice(0, 10);

// silly - try/catch in a function call
var error;

alert((function() {
  try {
    return nonexistent / void 0;
  } catch (error1) {
    error = error1;
    return `And the error is ... ${error}`;
  }
})());



// =====================
// Operators and Aliases
// =====================

// Javascript                 Coffeescript
===                        // is
!==                        // isnt
!                          // not
&&                         // and
||                         // or
true                       // true, yes, on
false                      // false, no, off
this                       // @, this
[].indexOf.call(b, a) >= 0 // a in b
a in b                     // a of b
for (a of b)               // for a from b
a ** b                     // a ** b
Math.floor(a / b)          // a // b
(a % b + b) % b            // a %% b

// dividend-dependent modulo (%%)
var modulo = function(a, b) { return (+a % (b = +b) + b) % b; };

-7 % 5 === -2; // The remainder of 7 / 5

modulo(-7, 5) === 3; // n %% 5 is always between 0 and 4

tabs.selectTabAtIndex(modulo(tabs.currentIndex - count, tabs.length));

// general examples
var volume, winner;

if (ignition === true) {
  launch();
}

if (band !== SpinalTap) {
  volume = 10;
}

if (answer !== false) {
  letTheWildRumpusBegin();
}

if (car.speed < limit) {
  accelerate();
}

if (pick === 47 || pick === 92 || pick === 13) {
  winner = true;
}

print(inspect(`My name is ${this.name}`));



// ========================
// The Existential Operator
// ========================

var footprints, solipsism, speed;

if ((typeof mind !== "undefined" && mind !== null) && (typeof world === "undefined" || world === null)) {
  solipsism = true;
}

speed = 0;

if (speed == null) {
  speed = 15;
}

footprints = typeof yeti !== "undefined" && yeti !== null ? yeti : "bear";

// loose comparison (!=) if compiler knows the variable is in scope
var major;

major = 'Computer Science';

if (major == null) {
  signUpForClass('Introduction to Wines');
}

// variable potentially undeclared, compiler does a thorough check
var environment;

if (typeof window !== "undefined" && window !== null) {
  environment = 'browser (probably)';
}

// chainable accessor variant
var ref, zip;

zip = typeof lottery.drawWinner === "function" ? (ref = lottery.drawWinner().address) != null ? ref.zipcode : void 0 : void 0;



// =======================
// Chaining Function Calls
// =======================

$('body').click(function(e) {
  return $('.box').fadeIn('fast').addClass('show');
}).css('background', 'white');



// ========================
// Destructuring Assignment
// ========================

var theBait, theSwitch;

theBait = 1000;

theSwitch = 0;

[theBait, theSwitch] = [theSwitch, theBait];

// functions with multiple return values
var city, forecast, temp, weatherReport;

weatherReport = function(location) {
  // Make an Ajax request to fetch the weather...
  return [location, 72, "Mostly Sunny"];
};

[city, temp, forecast] = weatherReport("Berkeley, CA");

// deeply nested properties
var city, futurists, name, sculptor, street;

futurists = {
  sculptor: "Umberto Boccioni",
  painter: "Vladimir Burliuk",
  poet: {
    name: "F.T. Marinetti",
    address: ["Via Roma 42R", "Bellagio, Italy 22021"]
  }
};

({sculptor} = futurists);

({
  poet: {
    name,
    address: [street, city]
  }
} = futurists);

// with splats
var close, contents, open, ref, tag,
  splice = [].splice;

tag = "<impossible>";

ref = tag.split(""), [open, ...contents] = ref, [close] = splice.call(contents, -1);

// ignoring parts of an array
var first, last, ref, text,
  slice = [].slice;

text = "Every literary critic believes he will outwit history and have the last word";

ref = text.split(" "), [first] = ref, [last] = slice.call(ref, -1);

// useful in class constructors
var Person, tim;

Person = class Person {
  constructor(options) {
    ({name: this.name, age: this.age, height: this.height = 'average'} = options);
  }

};

tim = new Person({
  name: 'Tim',
  age: 4
});



// ===========================
// Bound (Fat Arrow) Functions
// ===========================

var Account;

Account = function(customer, cart) {
  this.customer = customer;
  this.cart = cart;
  return $('.shopping_cart').on('click', (event) => {
    return this.customer.purchase(this.cart);
  });
};



// ===================
// Generator Functions
// ===================

var perfectSquares;

perfectSquares = function*() {
  var num;
  num = 0;
  while (true) {
    num += 1;
    yield num * num;
  }
};

window.ps || (window.ps = perfectSquares());

// iterating over a generator function
var fibonacci, getFibonacciNumbers;

fibonacci = function*() {
  var current, previous;
  [previous, current] = [1, 1];
  while (true) {
    [previous, current] = [current, previous + current];
    yield current;
  }
};

getFibonacciNumbers = function(length) {
  var n, ref, results;
  results = [1];
  ref = fibonacci();
  for (n of ref) {
    results.push(n);
    if (results.length === length) {
      break;
    }
  }
  return results;
};



// ===============
// Async Functions
// ===============

// Your browser must support async/await and speech synthesis to run this example.
var countdown, say, sleep;

sleep = function(ms) {
  return new Promise(function(resolve) {
    return window.setTimeout(resolve, ms);
  });
};

say = function(text) {
  window.speechSynthesis.cancel();
  return window.speechSynthesis.speak(new SpeechSynthesisUtterance(text));
};

countdown = async function(seconds) {
  var i, j, ref;
  for (i = j = ref = seconds; (ref <= 1 ? j <= 1 : j >= 1); i = ref <= 1 ? ++j : --j) {
    say(i);
    await sleep(1000); // wait one second
  }
  return say("Blastoff!");
};

countdown(3);



// =======
// Classes
// =======

var Animal, Horse, Snake, sam, tom;

Animal = class Animal {
  constructor(name) {
    this.name = name;
  }

  move(meters) {
    return alert(this.name + ` moved ${meters}m.`);
  }

};

Snake = class Snake extends Animal {
  move() {
    alert("Slithering...");
    return super.move(5);
  }

};
a.b.c.d.e = f
Horse = class Horse extends Animal {
  move() {
    alert("Galloping...");
    return super.move(45);
  }

};

sam = new Snake("Sammy the Python");

tom = new Horse("Tommy the Palomino");

sam.move();

tom.move();

// static methods
var Teenager;

Teenager = class Teenager {
  static say(speech) {
    var fillers, i, index, len, output, word, words;
    words = speech.split(' ');
    fillers = ['uh', 'um', 'like', 'actually', 'so', 'maybe'];
    output = [];
    for (index = i = 0, len = words.length; i < len; index = ++i) {
      word = words[index];
      output.push(word);
      if (index !== words.length - 1) {
        output.push(fillers[Math.floor(Math.random() * fillers.length)]);
      }
    }
    return output.join(', ');
  }

};



// ======================
// Prototypal Inheritance
// ======================

String.prototype.dasherize = function() {
  return this.replace(/_/g, "-");
};



// ================
// Switch/When/Else
// ================

switch (day) {
  case "Mon":
    go(work);
    break;
  case "Tue":
    go(relax);
    break;
  case "Thu":
    go(iceFishing);
    break;
  case "Fri":
  case "Sat":
    if (day === bingoDay) {
      go(bingo);
      go(dancing);
    }
    break;
  case "Sun":
    go(church);
    break;
  default:
    go(work);
}

// without a control expression
var grade, score;

score = 76;

grade = (function() {
  switch (false) {
    case !(score < 60):
      return 'F';
    case !(score < 70):
      return 'D';
    case !(score < 80):
      return 'C';
    case !(score < 90):
      return 'B';
    default:
      return 'A';
  }
})();

// grade == 'C'



// =================
// Try/Catch/Finally
// =================

var error;

try {
  allHellBreaksLoose();
  catsAndDogsLivingTogether();
} catch (error1) {
  error = error1;
  print(error);
} finally {
  cleanUp();
}



// ===================
// Chained Comparisons
// ===================

var cholesterol, healthy;

cholesterol = 127;

healthy = (200 > cholesterol && cholesterol > 60);



// =========================
// Block Regular Expressions
// =========================

var NUMBER;

NUMBER = /^0b[01]+|^0o[0-7]+|^0x[\da-f]+|^\d*\.?\d+(?:e[+-]?\d+)?/i; // binary
// octal
// hex
// decimal



// ========================
// Tagged Template Literals
// ========================

var greet, upperCaseExpr;

upperCaseExpr = function(textParts, ...expressions) {
  return textParts.reduce(function(text, textPart, i) {
    return text + expressions[i - 1].toUpperCase() + textPart;
  });
};

greet = function(name, adjective) {
  return upperCaseExpr`Hi ${name}. You look ${adjective}!`;
};



// =======
// Modules
// =======

import './local-file.coffee';

import 'coffeescript';

import _ from 'underscore';

import * as underscore from 'underscore';

import {
  now
} from 'underscore';

import {
  now as currentTimestamp
} from 'underscore';

import {
  first,
  last
} from 'underscore';

import utilityBelt, {
  each
} from 'underscore';

export default Math;

export var square = function(x) {
  return x * x;
};

export var Mathematics = class Mathematics {
  least(x, y) {
    if (x < y) {
      return x;
    } else {
      return y;
    }
  }

};

export {
  sqrt
};

export {
  sqrt as squareRoot
};

export {
  Mathematics as default,
  sqrt as squareRoot
};

export * from 'underscore';

export {
  max,
  min
} from 'underscore';

// dynamic import
(async function() {
  var run;
  ({run} = (await import('./browser-compiler-modern/coffeescript.js')));
  return run(`if 5 < new Date().getHours() < 9
  alert 'Time to make the coffee!'
else
  alert 'Time to get some work done.'`);
})();



// ===================
// Embedded JavaScript
// ===================

var hi;

hi = function() {
  return [document.title, "Hello JavaScript"].join(": ");
};

// escape backticks and backslashes with backslashes
var markdown;

markdown = function () {
  return `In Markdown, write code like \`this\``;
};

// block
function time() {
  return `The time is ${new Date().toLocaleTimeString()}`;
};

// ===
// JSX
// ===

var renderStarRating;

renderStarRating = function({rating, maxStars}) {
  var emptyStar, wholeStar;
  return <aside title={`Rating: ${rating} of ${maxStars} stars`}>
    {(function() {
    var i, ref, results;
    results = [];
    for (wholeStar = i = 0, ref = Math.floor(rating); (0 <= ref ? i < ref : i > ref); wholeStar = 0 <= ref ? ++i : --i) {
      results.push(<Star className="wholeStar" key={wholeStar} />);
    }
    return results;
  })()}
    {rating % 1 !== 0 ? <Star className="halfStar" /> : void 0}
    {(function() {
    var i, ref, ref1, results;
    results = [];
    for (emptyStar = i = ref = Math.ceil(rating), ref1 = maxStars; (ref <= ref1 ? i < ref1 : i > ref1); emptyStar = ref <= ref1 ? ++i : --i) {
      results.push(<Star className="emptyStar" key={emptyStar} />);
    }
    return results;
  })()}
  </aside>;
};

// ================
// Type Annotations
// ================

// @flow
/*::
type Obj = {
  num: number,
};
*/
var fn;

fn = function(str/*: string */, obj/*: Obj */)/*: string */ {
  return str + obj.num;
};
