# ========
# Overview
# ========

# Assignment:
number   = 42
opposite = true

# Conditions:
number = -42 if opposite

# Functions:
square = (x) -> x * x

# Arrays:
list = [1, 2, 3, 4, 5]

# Objects:
math =
  root:   Math.sqrt
  square: square
  cube:   (x) -> x * square x

# Splats:
race = (winner, runners...) ->
  print winner, runners

# Existence:
alert "I knew it!" if elvis?

# Array comprehensions:
cubes = (math.cube num for num in list)



# =========
# Functions
# =========

square = (x) -> x * x
cube   = (x) -> square(x) * x

# default arguments
fill = (container, liquid = "coffee") ->
  "Filling the #{container} with #{liquid}..."



# =======
# Strings
# =======

author = "Wittgenstein"
quote  = "A picture is a fact. -- #{ author }"

sentence = "#{ 22 / 7 } is a decent approximation of π"

# multiline
mobyDick = "Call me Ishmael. Some years ago --
  never mind how long precisely -- having little
  or no money in my purse, and nothing particular
  to interest me on shore, I thought I would sail
  about a little and see the watery part of the
  world..."

# block
html = """
       <strong>
         cup of coffeescript
       </strong>
       """



# ==================
# Objects and Arrays
# ==================

song = ["do", "re", "mi", "fa", "so"]

singers = {Jagger: "Rock", Elvis: "Roll"}

bitlist = [
  1, 0, 1
  0, 0, 1
  1, 1, 0
]

kids =
  brother:
    name: "Max"
    age:  11
  sister:
    name: "Ida"
    age:  9

# variable-as-key shorthand
name = "Michelangelo"
mask = "orange"
weapon = "nunchuks"
turtle = {name, mask, weapon}
output = "#{turtle.name} wears an #{turtle.mask} mask. Watch out for his #{turtle.weapon}!"



# ========
# Comments
# ========

###
Fortune Cookie Reader v1.0
Released under the MIT License
###

sayFortune = (fortune) ->
  console.log fortune # in bed!



# ===================================
# Lexical Scoping and Variable Safety
# ===================================

outer = 1
changeNumbers = ->
  inner = -1
  outer = 10
inner = changeNumbers()



# ===========================================
# If, Else, Unless and Conditional Assignment
# ===========================================

mood = greatlyImproved if singing

if happy and knowsIt
  clapsHands()
  chaChaCha()
else
  showIt()

date = if friday then sue else jill



# ========================================
# Splats, or Rest Parameters/Spread Syntax
# ========================================

gold = silver = rest = "unknown"

awardMedals = (first, second, others...) ->
  gold   = first
  silver = second
  rest   = others

contenders = [
  "Michael Phelps"
  "Liu Xiang"
  "Yao Ming"
  "Allyson Felix"
  "Shawn Johnson"
  "Roman Sebrle"
  "Guo Jingjing"
  "Tyson Gay"
  "Asafa Powell"
  "Usain Bolt"
]

awardMedals contenders...

alert """
Gold: #{gold}
Silver: #{silver}
The Field: #{rest.join ', '}
"""

# elide (merge) array elements
popular  = ['pepperoni', 'sausage', 'cheese']
unwanted = ['anchovies', 'olives']

all = [popular..., unwanted..., 'mushrooms']

# and object properties
user =
  name: 'Werner Heisenberg'
  occupation: 'theoretical physicist'

currentUser = { user..., status: 'Uncertain' }



# ========================
# Loops and Comprehensions
# ========================

# Eat lunch.
eat = (food) -> "#{food} eaten."
eat food for food in ['toast', 'cheese', 'wine']

# Fine five course dining.
courses = ['greens', 'caviar', 'truffles', 'roast', 'cake']
menu = (i, dish) -> "Menu Item #{i}: #{dish}"
menu i + 1, dish for dish, i in courses

# Health conscious meal.
foods = ['broccoli', 'spinach', 'chocolate']
eat food for food in foods when food isnt 'chocolate'

# loops
countdown = (num for num in [10..1])

# iterating objects
yearsOld = max: 10, ida: 9, tim: 11

ages = for child, age of yearsOld
  "#{child} is #{age}"

# while
# Econ 101
if this.studyingEconomics
  buy()  while supply > demand
  sell() until supply > demand

# Nursery Rhyme
num = 6
lyrics = while num -= 1
  "#{num} little monkeys, jumping on the bed.
    One fell out and bumped his head."

# closures
for filename in list
  do (filename) ->
    if filename not in ['.DS_Store', 'Thumbs.db', 'ehthumbs.db']
      fs.readFile filename, (err, contents) ->
        compile filename, contents.toString()



# ======================================
# Array Slicing and Splicing with Ranges
# ======================================

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

start   = numbers[0..2]

middle  = numbers[3...-2]

end     = numbers[-2..]

copy    = numbers[..]

# splicing
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

numbers[3..6] = [-3, -4, -5, -6]



# ===========================================================
# Everything is an Expression (at least, as much as possible)
# ===========================================================

grade = (student) ->
  if student.excellentWork
    "A+"
  else if student.okayStuff
    if student.triedHard then "B" else "B-"
  else
    "C"

eldest = if 24 > 21 then "Liz" else "Ike"

six = (one = 1) + (two = 2) + (three = 3)

# The first ten global properties.
globals = (name for name of window)[0...10]

alert(
  try
    nonexistent / undefined
  catch error
    "And the error is ... #{error}"
)

# assignment within expressions for new variables
six = (one = 1) + (two = 2) + (three = 3)

# statements as expressions
# The first ten global properties.
globals = (name for name of window)[0...10]

# silly - try/catch in a function call
alert(
  try
    nonexistent / undefined
  catch error
    "And the error is ... #{error}"
)



# =====================
# Operators and Aliases
# =====================

# Coffeescript     Javascript
is               # ===
isnt             # !==
not              # !
and              # &&
or               # ||
true, yes, on    # true
false, no, off   # false
@, this          # this
a in b           # [].indexOf.call(b, a) >= 0
a of b           # a in b
for a from b     # for (a of b)
a ** b           # a ** b
a // b           # Math.floor(a / b)
a %% b           # (a % b + b) % b

# dividend-depend modulo (%%)
-7 % 5 == -2 # The remainder of 7 / 5
-7 %% 5 == 3 # n %% 5 is always between 0 and 4

tabs.selectTabAtIndex((tabs.currentIndex - count) %% tabs.length)

# general examples
launch() if ignition is on

volume = 10 if band isnt SpinalTap

letTheWildRumpusBegin() unless answer is no

if car.speed < limit then accelerate()

winner = yes if pick in [47, 92, 13]

print inspect "My name is #{@name}"



# ========================
# The Existential Operator
# ========================

solipsism = true if mind? and not world?

speed = 0
speed ?= 15

footprints = yeti ? "bear"

# loose comparison (!=) if compiler knows the variable is in scope
major = 'Computer Science'

unless major?
  signUpForClass 'Introduction to Wines'

# variable potentially undeclared, compiler does a thorough check
if window?
  environment = 'browser (probably)'

# chainable accessor variant
zip = lottery.drawWinner?().address?.zipcode

# complete table
# Example              Definition
a?                   # tests that `a` is in scope and `a != null`
a ? b                # returns `a` if `a` is in scope and `a != null`; otherwise, `b`
a?.b or a?['b']      # returns `a.b` if `a` is in scope and `a != null`; otherwise, `undefined`
a?(b, c)             # returns the result of calling `a` (with arguments `b` and `c`) if `a` is in scope and callable; otherwise, `undefined`
a? b, c              # same as above
a ?= b               # assigns the value of `b` to `a` if `a` is not in scope or if `a == null`; produces the new value of `a`



# =======================
# Chaining Function Calls
# =======================

$ 'body'
.click (e) ->
  $ '.box'
  .fadeIn 'fast'
  .addClass 'show'
.css 'background', 'white'



# ========================
# Destructuring Assignment
# ========================

theBait   = 1000
theSwitch = 0

[theBait, theSwitch] = [theSwitch, theBait]

# functions with multiple return values
weatherReport = (location) ->
  # Make an Ajax request to fetch the weather...
  [location, 72, "Mostly Sunny"]

[city, temp, forecast] = weatherReport "Berkeley, CA"

# deeply nested properties
futurists =
  sculptor: "Umberto Boccioni"
  painter:  "Vladimir Burliuk"
  poet:
    name:   "F.T. Marinetti"
    address: [
      "Via Roma 42R"
      "Bellagio, Italy 22021"
    ]

{sculptor} = futurists

{poet: {name, address: [street, city]}} = futurists

# with splats
tag = "<impossible>"

[open, contents..., close] = tag.split("")

# ignoring parts of an array
text = "Every literary critic believes he will
        outwit history and have the last word"

[first, ..., last] = text.split " "

# useful in class constructors
class Person
  constructor: (options) ->
    {@name, @age, @height = 'average'} = options

tim = new Person name: 'Tim', age: 4


# ===========================
# Bound (Fat Arrow) Functions
# ===========================

Account = (customer, cart) ->
  @customer = customer
  @cart = cart

  $('.shopping_cart').on 'click', (event) =>
    @customer.purchase @cart



# ===================
# Generator Functions
# ===================

perfectSquares = ->
  num = 0
  loop
    num += 1
    yield num * num
  return

window.ps or= perfectSquares()

# iterating over a generator function
fibonacci = ->
  [previous, current] = [1, 1]
  loop
    [previous, current] = [current, previous + current]
    yield current
  return

getFibonacciNumbers = (length) ->
  results = [1]
  for n from fibonacci()
    results.push n
    break if results.length is length
  results



# ===============
# Async Functions
# ===============

sleep = (ms) ->
  new Promise (resolve) ->
    window.setTimeout resolve, ms

say = (text) ->
  window.speechSynthesis.cancel()
  window.speechSynthesis.speak new SpeechSynthesisUtterance text

countdown = (seconds) ->
  for i in [seconds..1]
    say i
    await sleep 1000 # wait one second
  say "Blastoff!"

countdown 3



# =======
# Classes
# =======

class Animal
  constructor: (@name) ->

  move: (meters) ->
    alert @name + " moved #{meters}m."

class Snake extends Animal
  move: ->
    alert "Slithering..."
    super 5

class Horse extends Animal
  move: ->
    alert "Galloping..."
    super 45

sam = new Snake "Sammy the Python"
tom = new Horse "Tommy the Palomino"

sam.move()
tom.move()

# static methods
class Teenager
  @say: (speech) ->
    words = speech.split ' '
    fillers = ['uh', 'um', 'like', 'actually', 'so', 'maybe']
    output = []
    for word, index in words
      output.push word
      output.push fillers[Math.floor(Math.random() * fillers.length)] unless index is words.length - 1
    output.join ', '


# ======================
# Prototypal Inheritance
# ======================

String::dasherize = ->
  this.replace /_/g, "-"



# ================
# Switch/When/Else
# ================

switch day
  when "Mon" then go work
  when "Tue" then go relax
  when "Thu" then go iceFishing
  when "Fri", "Sat"
    if day is bingoDay
      go bingo
      go dancing
  when "Sun" then go church
  else go work

# without a control expression
score = 76
grade = switch
  when score < 60 then 'F'
  when score < 70 then 'D'
  when score < 80 then 'C'
  when score < 90 then 'B'
  else 'A'
# grade == 'C'



# =================
# Try/Catch/Finally
# =================

try
  allHellBreaksLoose()
  catsAndDogsLivingTogether()
catch error
  print error
finally
  cleanUp()



# ===================
# Chained Comparisons
# ===================

cholesterol = 127

healthy = 200 > cholesterol > 60



# =========================
# Block Regular Expressions
# =========================

NUMBER     = ///
  ^ 0b[01]+    |              # binary
  ^ 0o[0-7]+   |              # octal
  ^ 0x[\da-f]+ |              # hex
  ^ \d*\.?\d+ (?:e[+-]?\d+)?  # decimal
///i



# ========================
# Tagged Template Literals
# ========================

upperCaseExpr = (textParts, expressions...) ->
  textParts.reduce (text, textPart, i) ->
    text + expressions[i - 1].toUpperCase() + textPart

greet = (name, adjective) ->
  upperCaseExpr"""
               Hi #{name}. You look #{adjective}!
               """



# =======
# Modules
# =======

import './local-file.coffee'
import 'coffeescript'

import _ from 'underscore'
import * as underscore from 'underscore'

import { now } from 'underscore'
import { now as currentTimestamp } from 'underscore'
import { first, last } from 'underscore'
import utilityBelt, { each } from 'underscore'

export default Math
export square = (x) -> x * x
export class Mathematics
  least: (x, y) -> if x < y then x else y

export { sqrt }
export { sqrt as squareRoot }
export { Mathematics as default, sqrt as squareRoot }

export * from 'underscore'
export { max, min } from 'underscore'

# dynamic import
do ->
  { run } = await import('./browser-compiler-modern/coffeescript.js')
  run '''
    if 5 < new Date().getHours() < 9
      alert 'Time to make the coffee!'
    else
      alert 'Time to get some work done.'
  '''


# ===================
# Embedded JavaScript
# ===================

hi = `function() {
  return [document.title, "Hello JavaScript"].join(": ");
}`

# escape backticks and backslashes with backslashes
markdown = `function () {
  return \`In Markdown, write code like \\\`this\\\`\`;
}`

# block
```
function time() {
  return `The time is ${new Date().toLocaleTimeString()}`;
}
```


# ===
# JSX
# ===

# XML elements are preserved, outputting JSX that can then be parsed and processed (ie. with `babel-react` plugin)
renderStarRating = ({ rating, maxStars }) ->
  <aside title={"Rating: #{rating} of #{maxStars} stars"}>
    {for wholeStar in [0...Math.floor(rating)]
      <Star className="wholeStar" key={wholeStar} />}
    {if rating % 1 isnt 0
      <Star className="halfStar" />}
    {for emptyStar in [Math.ceil(rating)...maxStars]
      <Star className="emptyStar" key={emptyStar} />}
  </aside>

# ================
# Type Annotations
# ================

# @flow

###::
type Obj = {
  num: number,
};
###

fn = (str ###: string ###, obj ###: Obj ###) ###: string ### ->
  str + obj.num
