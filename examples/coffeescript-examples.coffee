# Goal is to cover the minimal scope, and the majority of the common language features
# - https://www.sublimetext.com/docs/3/scope_naming.html#color_schemes

import { something, * } from 'something'
do -> { run } = await import('./browser-compiler-modern/coffeescript.js')
Something = React.lazy -> import('components/something')

try throw new Error 'failed' catch then finally
a = "something \x12 \'escaped singles\' \"escaped doubles\" #{interpolation} &middot; &"
-1 < 0 < 1 < 10 <= 100

{poet: {name, address: [street, city]}} = futurists
singers = Jagger: "Rock", Elvis: "Roll"
obj =
  withAt:   -> @::prop
  withThis: -> this::prop

grade = (student, period=(if b? then 7 else 6)) ->
  if student.excellentWork
    "A+"
  else if student.okayStuff
    if student.triedHard then "B" else "B-"
  else
    "C"

grade fred
grade(fred)

class Animal extends Being
  ###
    @options - annotation
  ###
  constructor: (options) ->
    super options
    { @name, @age, @height = 'average' } = options

  move: (meters) ->
    console.log this
    console.log @
    alert this.name + " moved #{meters}m."

dog = new Animal(name: 'sparky', age: 4)
dog.move 7

BestSomething =
  ({
    prop1 = 'default'
    prop2
    ...props
  }) ->
    return null unless prop2?

    renderProp2 = ->
      prop2.map (i) -> <div>{i}{Math.floor(i.thing) + i.otherThing}</div>

    <div className='& text-center'>
      {prop1}
      {renderProp2()}
    </div>

if \
 false \
      or \
 true
  d = 42

a = (arst) -> { arst, wbee} = arst

{
  poet: {
    name,
    address: [ street, city ],
 }
} = futurists

# yea... this mess is valid
b =
  arst: 'wee'
  fgph: (
    a ||
    b ||
    c ||
    d ||
    if a then (-> 'yay') else 'nay'
  )
  gphwpyu: alert # function... but not
  arstarst: -> alert()
