@requires_authorization(roles=["ADMIN"])
def somefunc(param1='', param2=0):
    r'''A docstring'''
    if param1 > param2: # interesting
        print 'Gre\'ater'
    return (param2 - param1 + 1 + 0b10l) or None

class SomeClass:
    pass

>>> message = '''interpreter
... prompt'''

class TestPipeline(BasePipeline):
  owner = 'tcui'
  schedule = 'never'
  retries = 5

  def __init__(self, dateid):
    # super(self.__class__ , self).__init__()
    self.dateid = dateid

  def run(self):
    print "RUNNING: Test Pipeline"

    ### Queries ###

    a = {
      arst: 'weee',
      arst2: 'weee',
    }

    test_users_wf = WaitforOperator({
      'deps': [],
      'table_name': 'users_scrape',
      'partitions': [self.dateid]
    }).run()

    dumb_users_create = CreateTableOperator({
      'deps': [],
      'table_name': 'dumb_users',
      'schema': queries.dumb_users_schema,
      'date_partitioned': True
    }).run()

    dumb_users = QueryOperator({
      'deps': [],
      'query': queries.dumb_users,
      'dateid': self.dateid,
      'dest_table': 'dumb_users',
      'dest_partition': self.dateid
    }).run()

    dumb_users_drop = DropTableOperator({
      'deps': [],
      'table_name': 'dumb_users'
    })
