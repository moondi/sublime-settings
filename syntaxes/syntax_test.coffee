# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

if \
#  ^ punctuation.separator.continuation
 false \
#      ^ punctuation.separator.continuation
      or \
#        ^ punctuation.separator.continuation
 true
  d = 42

# embedded-javascript
hi = `function() { return [document.title, "Hello &middot; JavaScript\?"].join(": "); }`
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.script
#    ^ punctuation.definition.string.begin
#                                                 ^^^^^^^^ string.quoted.script constant.character.entity
#                                                                    ^^ constant.character.escape
#                                                                                      ^ punctuation.definition.string.end

### @something something ###
#<- comment.block punctuation.definition.comment.begin
#^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.block
#^^ punctuation.definition.comment.begin
#   ^^^^^^^^^^ storage.type.annotation
#                        ^^^ punctuation.definition.comment.end

# something
#<- comment.line.number-sign punctuation.definition.comment
#^^^^^^^^^^ comment.line.number-sign

OPERATOR = /// ^ (
#          ^^^^^^^ string.regexp.block
#          ^^^ punctuation.definition.regex.begin
  ?: [-=]>             # function
#^^^^^^^^^ string.regexp.block
#                      ^^^^^^^^^^ comment.line.number-sign
#                      ^ comment.line.number-sign punctuation.definition.comment
   | [-+*/%<>&|^!?=]=  # compound assign / compare
   | >>>=?             # zero-fill right shift
   | ([-+:])\1         # doubles
   | ([&|<>])\2=?      # logic / shift
   | \?\.              # soak access
   | \.{2,3}
   | #{this_is_possible_too}
#^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.regexp.block
#    ^^^^^^^^^^^^^^^^^^^^^^^ source.coffee.embedded
#    ^^ punctuation.section.embedded.begin
#                          ^ punctuation.section.embedded.end
///
#<- string.regexp.block punctuation.definition.regex.end
#^^ string.regexp.block punctuation.definition.regex.end

regex = /asdf/g
#       ^^^^^^^ string.regexp

a = 1
#   ^ constant.numeric
