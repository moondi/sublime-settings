# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

arst = \
#      ^ punctuation.separator.continuation
  'wee'

# illegal-tag
<  > < /> <  />
#<- invalid.illegal.tag.incomplete
#^^^ invalid.illegal.tag.incomplete
#    ^^^^ invalid.illegal.tag.incomplete
#         ^^^^^ invalid.illegal.tag.incomplete

# fragment
<></>
#<- meta.tag.fragment punctuation.definition.tag.begin
#^^^^ meta.tag.fragment
#^ punctuation.definition.tag.begin
# ^^^ punctuation.definition.tag.end

# self-closing
<div/>
#<- meta.tag.open punctuation.definition.tag.begin
#^^^^^ meta.tag.open
#^^^ entity.name.tag.open
#   ^^ punctuation.definition.tag.end

<div    />
#<- meta.tag.open punctuation.definition.tag.begin
#^^^ meta.tag.open
#^^^ entity.name.tag.open
#   ^^^^ source
#       ^^ meta.tag.open punctuation.definition.tag.end

# Test that a self-closing tag can span multiple lines
<div
#<- meta.tag.open punctuation.definition.tag.begin
#^^^ meta.tag.open entity.name.tag.open
/>
#<- meta.tag.open punctuation.definition.tag.end
#^ meta.tag.open punctuation.definition.tag.end

<div></div>
#<- meta.tag.open punctuation.definition.tag.begin
#^^^^ meta.tag.open
#^^^ entity.name.tag.open
#   ^ punctuation.definition.tag.end
#    ^^^^^^ meta.tag.close
#    ^^ punctuation.definition.tag.begin
#      ^^^ entity.name.tag.close
#         ^ punctuation.definition.tag.end

# Test that nesting stacks scopes appropriately
<div><span><i/><span><br/></span></span></div>
#<- meta.tag.open
#^^^^ meta.tag.open
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents
#    ^^^^^^ meta.tag.open
#          ^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents meta.tag.contents
#          ^^^^ meta.tag.open
#              ^^^^^^ meta.tag.open
#                    ^^^^^ meta.tag.contents meta.tag.contents meta.tag.contents meta.tag.open
#                         ^^^^^^^ meta.tag.close
#                                ^^^^^^^ meta.tag.close
#                                       ^^^^^^ meta.tag.close

# Specifically test that quotes within a set of tags are not scoped at all
<div>hi "quotes should be normal text" 'so should these quotes'</div>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents
#       ^ - string - punctuation
#                                      ^ - string - punctuation

<div attr1='wee' attr2="wee" attr3={wee}/>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.attribute
#    ^^^^^ entity.other.attribute-name
#         ^ keyword.operator.assignment
#          ^^^^^ string.quoted.single
#                ^^^^^ entity.other.attribute-name
#                     ^ keyword.operator.assignment
#                      ^^^^^ string.quoted.double
#                            ^^^^^ entity.other.attribute-name
#                                 ^ keyword.operator.assignment
#                                  ^^^^^ source.coffee.embedded.source

# escaped-entity
<div>\x12</div>
#    ^^^^ meta.tag.contents constant.character.escape

# html-entity
<div>& &middot;</div>
#    ^^^^^^^^^^ meta.tag.contents
#    ^ invalid.illegal.bad-ampersand
#      ^^^^^^^^ constant.character.entity
#      ^ punctuation.definition.entity.begin
#             ^ punctuation.definition.entity.end

# embedded-cjsx
<div>{@renderSomethingElse()}</div>
#    ^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents source.coffee.embedded.source
#    ^ meta.brace.curly punctuation.section.embedded.begin
#     ^ meta.function-call variable.language.this
#      ^^^^^^^^^^^^^^^^^^^^^ meta.function-call
#      ^^^^^^^^^^^^^^^^^^^ variable.function
#                         ^ punctuation.section.group.begin
#                          ^ punctuation.section.group.end
#                           ^ meta.brace.curly punctuation.section.embedded.end

# embedded-cjsx
<div>{this.renderSomethingElse()}</div>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents source.coffee.embedded.source
#    ^ meta.brace.curly punctuation.section.embedded.begin
#     ^^^^ meta.function-call variable.language.this
#         ^ punctuation.accessor
#          ^^^^^^^^^^^^^^^^^^^^ meta.function-call
#          ^^^^^^^^^^^^^^^^^^ variable.function
#                             ^ punctuation.section.group.begin
#                              ^ punctuation.section.group.end
#                               ^ meta.brace.curly punctuation.section.embedded.end

<div>
  {
#^^ meta.tag.contents
# ^ source.coffee.embedded.source meta.brace.curly punctuation.section.embedded.begin
    # renders the collection
#^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents source.coffee.embedded.source
#   ^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.number-sign
    @props.collection.map (item, index) =>
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents source.coffee.embedded.source
#   ^^^^^^ variable.other.readwrite.instance
#         ^ meta.delimiter.method.period
#                    ^ meta.delimiter.method.period
#                     ^^^ support.function.method.array
#                                       ^^ keyword.declaration.function
      <div key={index} className="p16 #{'m16' if @props.extraSpacing}">{item.name}</div>
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.contents source.coffee.embedded.source
#     ^^^^ meta.tag.open
#      ^^^ entity.name.tag.open
#          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.attribute
#                      ^^^^^^^^^ entity.other.attribute-name
#                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double
#                                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ source.coffee.embedded.source
#                                     ^^ punctuation.section.embedded.begin
#                                             ^^ keyword.control.conditional
#                                                ^^^^^^ variable.other.readwrite.instance
  }
#^^ meta.tag.contents source.coffee.embedded.source
# ^ meta.brace.curly punctuation.section.embedded.end
</div>

# embedded-coffee
<div attr="something #{something + @interpolated}"/>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.attribute
#        ^ keyword.operator.assignment
#         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double
#         ^ punctuation.definition.string.begin
#                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ source.coffee.embedded.source
#                    ^^ punctuation.section.embedded.begin
#                                  ^^^^^^^^^^^^^ variable.other.readwrite.instance
#                                               ^ punctuation.section.embedded.end
#                                                ^ punctuation.definition.string.end

# single-quoted-string
<div attr='something \'with &middot; quotes\' #{not interpolated}'/>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.attribute
#        ^ keyword.operator.assignment
#         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single
#         ^ punctuation.definition.string.begin
#                    ^^ constant.character.escape
#                           ^^^^^^^^ constant.character.entity
#                                             ^^^^^^^^^^^^^^^^^^^ - source.coffee.embedded.source
#                                                                ^ punctuation.definition.string.end

# double-quoted-string
<div attr="something \"with &middot; quotes\" #{interpolated}"/>
#    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.tag.attribute
#        ^ keyword.operator.assignment
#         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double
#         ^ punctuation.definition.string.begin
#                    ^^ constant.character.escape
#                           ^^^^^^^^ constant.character.entity
#                                             ^^^^^^^^^^^^^^^ source.coffee.embedded.source
#                                                            ^ punctuation.definition.string.end


# comments in JSX
<div
  # ... this should be a comment too
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.number-sign
  attr={1} # comments are actually allowed here
#          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.number-sign
>
  # this is rendered text, not a comment
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ -comment.line.number-sign
</div>
