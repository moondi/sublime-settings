# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

# this should be pretty easy to do
result = square(7)
#        ^^^^^^^^^ meta.function-call
#        ^^^^^^ variable.function

# this... might be a lot harder to do...
result = square 7
#        ^^^^^^^^ meta.function-call
#        ^^^^^^ variable.function

window.setTimeout(square, 500)
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call
#      ^^^^^^^^^^ support.function
#                 ^^^^^^ variable.function

_.flatMap store.standard_groups[key], (sg) ->
  [ _.assign({}, sg, r: 'sg'), _.flatMap(sg.standards, (s) -> [ _.assign({}, s, r: 'std'), s.objectives ]) ]
#     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call
#                                      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call
#                                                                 ^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call meta.function-call
#     ^^^^^^ variable.function
#                                ^^^^^^^ variable.function
#                                                                 ^^^^^^ variable.function
