# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

@something
#<- variable.other.readwrite.instance variable.language.this
#^^^^^^^^^ variable.other.readwrite.instance

this.something
#<- variable.other.readwrite.instance variable.language.this
#^^^ variable.language.this
#   ^ punctuation.accessor - variable.language.this
#^^^^^^^^^^^^^ variable.other.readwrite.instance

a:
#<- meta.mapping.key
#<- - meta.mapping.value
#^ punctuation.separator.key-value.coffee

  1
# ^ meta.mapping.value.coffee

a = 1
#<- variable.other.readwrite
# ^ keyword.operator.assignment
#   ^ constant.numeric

b =
  2
# ^ constant.numeric

c =

  3
# ^ constant.numeric

obj =
#<- variable.other.readwrite
#^^ variable.other.readwrite
#   ^ keyword.operator.assignment
  withAt:   -> @::prop
#              ^ variable.language.this
#               ^^ keyword.operator.prototype
#                 ^^^^ variable.other.readwrite.instance
  withThis: -> this::prop
#              ^^^^ variable.language.this
#                  ^^ keyword.operator.prototype
#                    ^^^^ variable.other.readwrite.instance

singers = Jagger: "Rock", Elvis: "Roll"
#<- variable.other.readwrite
#^^^^^^ variable.other.readwrite
#       ^ keyword.operator.assignment
