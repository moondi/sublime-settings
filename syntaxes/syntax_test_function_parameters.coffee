# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

# with default values
fill = (container, liquid = "coffee") -> doStuff()
# ^^^^^ meta.function
#      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters
#      ^ punctuation.section.group.begin
#       ^^^^^^^^^ variable.parameter
#                ^ punctuation.separator
#                  ^^^^^^ variable.parameter
#                         ^ keyword.operator.assignment
#                           ^^^^^^^^^ string.quoted.double
#                           ^ punctuation.definition.string.begin
#                                  ^ punctuation.definition.string.begin
#                                   ^ punctuation.section.group.end
#                                    ^^^ meta.function
#                                     ^^ keyword.declaration.function
#                                        ^^^^^^^^^ meta.function-call.coffee
#                                        ^^^^^^^ variable.function.coffee

# ===============
# bound functions
# ===============
$('.shopping_cart').on 'click', (event) => @customer.purchase @cart
#                   ^^ - variable.parameter
#                               ^^^^^^^ meta.function.parameters
#                               ^ punctuation.section.group.begin
#                                ^^^^^ variable.parameter
#                                     ^ punctuation.section.group.end
#                                      ^^^ meta.function
#                                       ^^ keyword.declaration.function.bound

Something2 = ({
#<- meta.function entity.name.function
#^^^^^^^^^^^^^^^ meta.function
#^^^^^^^^^ entity.name.function
#            ^ punctuation.section.group.begin
#             ^ meta.variable.assignment.destructured punctuation.definition.braces.begin keyword.operator
  prop1 = 'default'
# ^^^^^^^^^^^^^^^^^ meta.function meta.variable.assignment.destructured
# ^^^^^ variable.parameter
#       ^ keyword.operator.assignment
#         ^^^^^^^^^ string.quoted.single
  prop2
# ^^^^^ meta.function meta.variable.assignment.destructured variable.parameter
  isThing = true
#           ^^^^ constant.language.boolean.true
  numStuff = 4
#            ^ constant.numeric
}) ->
#<- meta.function.parameters meta.variable.assignment.destructured punctuation.definition.braces.end keyword.operator
#^ meta.function.parameters punctuation.section.group.end
# ^^^^ meta.function
#  ^^ keyword.declaration.function
  something

grade = (student, period=(if b? then 7 else 6)) ->
#<- meta.function entity.name.function
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function
#^^^^ entity.name.function
#     ^ keyword.operator.assignment
#       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters
#        ^^^^^^^ variable.parameter
#                 ^^^^^^ variable.parameter
#                       ^ keyword.operator.assignment
  if student.excellentWork then "A+" else 'C'

# yea... this mess is valid
b =
  arst: 'wee'
  fgph: (
# ^^^^ - entity.name.function
    a ||
    b ||
    c ||
    d ||
    if a then (-> 'yay') else 'nay'
  )
  gphwpyu: alert # function... but not
  arstarst: -> alert()

a = (arst, { a, b }, c = {d: 1}, [e, f, g], h) ->
#<- meta.function.definition entity.name.function
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function
#^^^ meta.function.definition
# ^ keyword.operator.assignment
#   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters
#   ^ punctuation.section.group.begin
#    ^^^^ variable.parameter.function
#        ^ punctuation.separator.comma
#          ^^^^^^^^ meta.variable.assignment.destructured
#          ^ keyword.operator.assignment.destructured.object
#            ^ variable.other.readwrite
#               ^ variable.other.readwrite
#                 ^ keyword.operator.assignment.destructured.object
#                  ^ punctuation.separator.comma
#                    ^ variable.parameter.function
#                      ^ keyword.operator.assignment.default
#                        ^ -keyword.operator.assignment
#                        ^ punctuation.section.block.begin
#                             ^ -keyword.operator.assignment
#                             ^ punctuation.section.block.end
#                              ^ punctuation.separator.comma
#                                ^^^^^^^^^ meta.variable.assignment.destructured
#                                ^ keyword.operator.assignment.destructured.array
#                                 ^ variable.other.readwrite
#                                  ^ punctuation.separator.comma
#                                    ^ variable.other.readwrite
#                                     ^ punctuation.separator.comma
#                                       ^ variable.other.readwrite
#                                        ^ keyword.operator.assignment.destructured.array
#                                         ^ punctuation.separator.comma
#                                           ^ variable.parameter.function
#                                            ^ punctuation.section.group.end
#                                              ^^ keyword.declaration.function

a = (arst, [e, f, g], c = {d: 1}, { a, b }, h, g = [1, 2, 3]) ->

a = (
  arst
  [e, f, g]
  c = {d: 1}
  { a, b }
  [i, j ]
  h
  k = [1, 2, 3]
) -> h

a = (
  arst
  [e, f, g]
  c = {
    d: 1
    z: -> 2
  }
  d = -> 'hi'
# ^ entity.name.function
  { a, b }
  h = "#{arstarst}ars a"
  [i, j ]
  # comments should work
# ^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters comment.line.number-sign
  k = [1, 2, 3]
  l = 123
  m = ->
    'bye'
  n = (true || false && !BANANA)
# ^ variable.parameter.function
  o = (what, are, you, doing) -> 'bye'
# ^ entity.name.function
  p = (
# ^ entity.name.function
    you
    are
    insane
  ) ->
    'really, WTF'
  z
# ^ variable.parameter.function
) -> f

# this is kinda bonkers and weird (particularly w/'stuff'), but is valid
crazy = (
  arst: 'aaa'
  a = {
    arst
    wee
  } = stuff
  b = [1,2,3]
  c = -> 'hi'
  d = [
    1
    2
    3
  ]
) -> console.log a, b, c(), d, stuff, arst, wee
stuff = {arst: 'arst', wee: 'wee'}
crazy() # { arst: 'arst', wee: 'wee' } [ 1, 2, 3 ] 'hi' [ 1, 2, 3 ] { arst: 'arst', wee: 'wee' } 'arst' 'wee'

BestSomething =
#<- meta.function.definition entity.name.function
#^^^^^^^^^^^^^^ meta.function.definition
#^^^^^^^^^^^^ entity.name.function
  ({
    prop1 = 'default'
    prop2
  }) ->
    <div className='text-center'>
      {prop1}
    </div>

(
 a = 1
) -> stuff
