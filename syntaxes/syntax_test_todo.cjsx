# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

# ================================================================
#
# dumping ground for everything I find while working
# - minimal repro is preferred, but actual snippets are fine too
# - each should have a test or tests for the incorrect portion
#
# ================================================================

StandardInstanceableTag = ({
  standard_instance
# ^^^^^^^^^^^^^^^^^ variable.parameter.function
# I wasn't sure if I wanted this on the weekend, now I know --- yes
  units
  selectedUnitIds
  shouldInitTooltip = true # disabling means that a parent component should be manually initializing for performance reasons
#                     ^^^^ constant.language.boolean.true
#                     I just forgot about this case, shouldn't be hard to add in
}) ->


Something = ->
  <div onClick={(e) -> console.log 'broken'}/>
#               ^^^^^^^^^^^^^^^^^^^^^^^^^^^ source.coffee.embedded.source
#                   ^^ keyword.declaration.function


_.map _.sortBy(@props.objective_instances, 'ordinal'), (objective_instance, index) =>
  rowClass = 'border-top' unless index is 0
  <ObjectiveInstanceRow key={objective_instance.id} className={rowClass} objective_instance={objective_instance}/>
# ^^^^^^^^^^^^^^^^^^^^^ meta.tag.open


someFunction(
  <div>stuff</div>
# ^^^^^ meta.tag.open
)


_.map collection, (item, index) ->
  <Component prop1={item.prop1} prop2={someFunc?(item.thing) > 0} prop3={item.prop3}/>
#                                                                 ^^^^^ meta.tag.attribute entity.other.attribute-name


SOME_CONST = 32
#            ^^ constant.numeric
ThingFuckingUpConstantHighlighting =
  ({
  }) -> 1


openOverlay: (lesson_id = null) => @setState isOverlayOpen: true, defaultLessonId: lesson_id
arst
#^^^ - meta.function.parameters variable.parameter.function


@index: (params) ->
  fetch "#{Hrefs.Api}/recipes.json",
    method: 'GET'
#   ^^^^^^ - meta.function.definition entity.name.function
@show: (params) ->
  $.get "#{Hrefs.Api}/recipes/#{params.recipe_id}.json",

export class CreateScaleConfirmModal extends React.PureComponent
#      ^^^^^ meta.class storage.type.class

export default class CreateScaleConfirmModal extends React.PureComponent
#              ^^^^^ meta.class storage.type.class

class CreateScaleConfirmModal extends React.PureComponent
  @defaultProps:
# ^ variable.other.readwrite.instance.coffee variable.language.this.coffee
#  ^^^^^^^^^^^^ variable.other.readwrite.instance
    isSaving: false
