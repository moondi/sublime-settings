# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

eat = (food) -> "#{food} eaten."
eat food for food in ['broccoli', 'spinach', 'chocolate'] when food isnt 'chocolate'
#        ^^^ keyword.control.loop
#                 ^^ keyword.control.loop
#                                                         ^^^^ keyword.control.conditional

ages = for child, age of yearsOld
#      ^^^ keyword.control.loop
#                     ^^ keyword.control.loop
  "#{child} is #{age}"

i for i in [a..b] by c
# ^^^ keyword.control.loop
#       ^^ keyword.control.loop
#                 ^^ keyword.control.loop

buy() while supply > demand
#     ^^^^^ keyword.control.loop
sell() until supply > demand
#      ^^^^^ keyword.control.loop

for filename in list
#<- keyword.control.loop
#^^ keyword.control.loop
#            ^^ keyword.control.loop
  do (filename) -> fs.readFile filename, (err, contents) -> compile filename, contents.toString()
# ^^ keyword.declaration.function.self-invoking
#               ^^ keyword.declaration.function

own = (value for own key, value of whiskers)
#            ^^^^^^^ keyword.control.loop
#                               ^^ keyword.control.loop

if a then b else if c then d else e
#<- keyword.control.conditional
#^ keyword.control.conditional
#    ^^^^ keyword.control.conditional
#           ^^^^ keyword.control.conditional
#                ^^ keyword.control.conditional
#                     ^^^^ keyword.control.conditional
#                            ^^^^ keyword.control.conditional

switch a when 'b' then 'c' else 'd'
#<- keyword.control.conditional
#^^^^^ keyword.control.conditional
#        ^^^^ keyword.control.conditional
#                 ^^^^ keyword.control.conditional
#                          ^^^^ keyword.control.conditional

switch a
#<- keyword.control.conditional
#^^^^^ keyword.control.conditional
  when 'b' then 'c'
# ^^^^ keyword.control.conditional
#          ^^^^ keyword.control.conditional
  else 'd'
# ^^^^ keyword.control.conditional

b unless c
# ^^^^^^ keyword.control.conditional

ns = for n in [0..99]
#    ^^^ keyword.control.loop
#          ^^ keyword.control.loop
  if n > 9
# ^^ keyword.control.conditional
    break
#   ^^^^^ keyword.control.flow
  else if n & 1
# ^^^^ keyword.control.conditional
#      ^^ keyword.control.conditional
    continue
#   ^^^^^^^^ keyword.control.flow
  else
# ^^^^ keyword.control.conditional
    return n
#   ^^^^^^ keyword.control.flow

if a then debugger
#         ^^^^^^^^ keyword.control.flow

try throw new Error 'failed' catch then finally
#<- keyword.control.exception
#^^ keyword.control.exception
#   ^^^^^ keyword.control.exception
#                            ^^^^^ keyword.control.exception
#                                  ^^^^ keyword.control.conditional
#                                       ^^^^^^^ keyword.control.exception
