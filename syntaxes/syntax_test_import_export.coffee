# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

# this was regression from when I initially hacked in support for import/export
# by largely copying from javascript's sublime-syntax
a = -> somethingEndingIn_as
#                        ^^ - keyword.control.import-export

# ======
# import
# ======
import 'coffeescript'
#<- meta.import keyword.control.import-export
#^^^^^^^^^^^^^^^^^^^^ meta.import
#^^^^^ keyword.control.import-export
#      ^^^^^^^^^^^^^^ string.quoted.single
#      ^ punctuation.definition.string.begin
#                   ^ punctuation.definition.string.end

import * as underscore from 'underscore'
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.import
#      ^ constant.other
#        ^^ keyword.control.import-export
#           ^^^^^^^^^^ variable.other.readwrite
#                      ^^^^ keyword.control.import-export
#                           ^^^^^^^^^^^^ string.quoted.single
#                           ^ punctuation.definition.string.begin
#                                      ^ punctuation.definition.string.end

import utilityBelt, { each, now as currentTimestamp } from 'underscore'
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.import
#      ^^^^^^^^^^^ variable.other.readwrite
#                 ^ punctuation.separator.comma
#                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.block
#                   ^ punctuation.section.block.begin
#                     ^^^^ variable.other.readwrite
#                         ^ punctuation.separator.comma
#                           ^^^ variable.other.readwrite.coffee
#                               ^^ keyword.control.import-export
#                                  ^^^^^^^^^^^^^^^^ variable.other.readwrite.coffee
#                                                   ^ punctuation.section.block.end
#                                                     ^^^^ keyword.control.import-export
#                                                          ^^^^^^^^^^^^ string.quoted.single
#                                                          ^ punctuation.definition.string.begin
#                                                                     ^ punctuation.definition.string.end

import utilityBelt, # comment should not break highlighting
  { each, now as currentTimestamp } from 'underscore'
#   ^^^^ meta.import variable.other.readwrite

import {
  map as _map
# ^^^^^^^^^^^^ meta.import
# ^^^ variable.other.readwrite
#     ^^ keyword.control.import-export
#        ^^^^ variable.other.readwrite
  # comment should not break highlighting
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.import comment.line.number-sign.coffee
  assign as _assign
# ^^^^^^^^^^^^^^^^^ meta.import
# ^^^^^^ variable.other.readwrite
#        ^^ keyword.control.import-export
#           ^^^^^^^ variable.other.readwrite
} from 'lodash-es'

# ======
# export
# ======
export default Math
#<- meta.export keyword.control.import-export
#^^^^^^^^^^^^^ meta.export
#^^^^^ keyword.control.import-export
#      ^^^^^^^ keyword.control.import-export

export { Mathematics as default, sqrt as squareRoot }
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.export
#      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.block
#      ^ punctuation.section.block.begin
#        ^^^^^^^^^^^ variable.other.readwrite
#                    ^^ keyword.control.import-export
#                       ^^^^^^^ keyword.control.import-export
#                              ^ punctuation.separator.comma
#                                ^^^^ variable.other.readwrite
#                                     ^^ keyword.control.import-export
#                                        ^^^^^^^^^^ variable.other.readwrite
#                                                   ^ punctuation.section.block.end

export * from 'underscore'
#^^^^^^^^^^^^^^^^^^^^^^^^^ meta.export
#      ^ constant.other
#        ^^^^ keyword.control.import-export
#             ^^^^^^^^^^^^ string.quoted.single
#             ^ punctuation.definition.string.begin
#                        ^ punctuation.definition.string.end

export {
#^^^^^^^ meta.export
#      ^ meta.block punctuation.definition.block.begin
  max as default
# ^^^^^^^^^^^^^^ meta.export meta.block
# ^^^ variable.other.readwrite
#     ^^ keyword.control.import-export
#        ^^^^^^^ keyword.control.import-export
  min
# ^^^ meta.export meta.block variable.other.readwrite
  # comments should work
# ^^^^^^^^^^^^^^^^^^^^^^^ meta.export comment.line.number-sign
  map as _map
# ^^^^^^^^^^^ meta.export meta.block
# ^^^ variable.other.readwrite
#     ^^ keyword.control.import-export
#        ^^^^ variable.other.readwrite
} from 'underscore'
#<- meta.export meta.block punctuation.definition.block.end
#^^^^^^^^^^^^^^^^^^ meta.export
# ^^^^ keyword.control.import-export
#      ^^^^^^^^^^^^ string.quoted.single
#      ^ punctuation.definition.string.begin
#                 ^ punctuation.definition.string.end

# ==============
# dynamic import
# ==============
do -> { run } = await import('./browser-compiler-modern/coffeescript.js')
#<- keyword.control
#^ keyword.control
#  ^^ storage.type.function
#     ^^^^^^^^^ meta.variable.assignment.destructured
#     ^ keyword.operator.assigment puntuation.definition.block.begin
#       ^^^ variable.other.readwrite
#           ^ keyword.operator.assigment puntuation.definition.block.end
#             ^ keyword.operator.assignment
#               ^^^^^ keyword.control.flow.await
#                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.import.dynamic
#                     ^^^^^^^ keyword.control.import-export
#                           ^ punctuation.section.group.begin
#                            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single
#                            ^ punctuation.definition.string.begin
#                                                                      ^ punctuation.definition.string.end
#                                                                       ^ keyword.control.import-export
#                                                                       ^ punctuation.section.group.begin

Something = React.lazy -> import('components/something')
# <- variable.other.readwrite
#^^^^^^^^ variable.other.readwrite
#         ^ keyword.operator.assignment
#                      ^^ storage.type.function
#                         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.import.dynamic
#                         ^^^^^^^ keyword.control.import-export
#                               ^ punctuation.section.group.begin
#                                ^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single
#                                ^ punctuation.definition.string.begin
#                                                     ^ punctuation.definition.string.end
#                                                      ^ keyword.control.import-export
#                                                      ^ punctuation.definition.group.end
