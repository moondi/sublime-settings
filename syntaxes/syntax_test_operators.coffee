# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

a = 1
# ^ keyword.operator.assignment

# a === b && a2 === b2 && (c !== d || c2 !== d2 || !e || !f)
a is b and a2 == b2 && (c isnt d or c2 != d2 || not e || !f)
# ^^ keyword.operator.comparison
#      ^^^ keyword.operator.logical
#             ^^ keyword.operator.comparison
#                   ^^ keyword.operator.logical
#                         ^^^^ keyword.operator.comparison
#                                ^^ keyword.operator.logical
#                                      ^^ keyword.operator.comparison
#                                            ^^ keyword.operator.logical
#                                               ^^^ keyword.operator.logical
#                                                     ^^ keyword.operator.logical
#                                                        ^ keyword.operator.logical

a = and: 1, or: 2, is: 3 isnt: 4, not: 5
#   ^^^^ - keyword.operator
#           ^^^ - keyword.operator
#                  ^^^ - keyword.operator
#                        ^^^^^ - keyword.operator
#                                 ^^^^ - keyword.operator

# chained comparisons
100 >= 10 > 1 > 0 > -1
#   ^^ keyword.operator.comparison
#         ^ keyword.operator.comparison
#             ^ keyword.operator.comparison
#                 ^ keyword.operator.comparison

-1 < 0 < 1 < 10 <= 100
#  ^ keyword.operator.comparison
#      ^ keyword.operator.comparison
#          ^ keyword.operator.comparison
#               ^^ keyword.operator.comparison

true is not false is true isnt false
#    ^^ keyword.operator.comparison
#       ^^^ keyword.operator.logical
#                 ^^ keyword.operator.comparison
#                         ^^^^ keyword.operator.comparison

0 == 0 != 1 == 1
# ^^ keyword.operator.comparison
#      ^^ keyword.operator.comparison
#           ^^ keyword.operator.comparison

a ** b * c
# ^^ keyword.operator.arithmetic
#      ^ keyword.operator.arithmetic

a // b / c
# ^^ keyword.operator.arithmetic
#      ^ keyword.operator.arithmetic

a %% b % c
# ^^ keyword.operator.arithmetic
#      ^ keyword.operator.arithmetic

a + b - -c
# ^ keyword.operator.arithmetic
#     ^ keyword.operator.arithmetic
#       ^ keyword.operator.arithmetic

# existential operator
a?
#^ keyword.operator.existential

a ? b                # returns `a` if `a` is in scope and `a != null`; otherwise, `b`
# ^ keyword.operator.existential

a?.b or a?['b']      # returns `a.b` if `a` is in scope and `a != null`; otherwise, `undefined`
#^ keyword.operator.existential
#        ^ keyword.operator.existential

a?(b, c)             # returns the result of calling `a` (with arguments `b` and `c`) if `a` is in scope and callable; otherwise, `undefined`
#^ keyword.operator.existential

a? b, c              # same as above
#^ keyword.operator.existential

a ?= b               # assigns the value of `b` to `a` if `a` is not in scope or if `a == null`; produces the new value of `a`
# ^^ keyword.operator.assignment.existential

true, yes, on
#<- constant.language.boolean.true
#^^^ constant.language.boolean.true
#     ^^^ constant.language.boolean.true
#          ^^ constant.language.boolean.true

false, no, off
#<- constant.language.boolean.false
#^^^^ constant.language.boolean.false
#      ^^ constant.language.boolean.false
#          ^^^ constant.language.boolean.false

@, this
#<- variable.language.this
#  ^^^^ variable.language.this
