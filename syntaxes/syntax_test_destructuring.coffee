# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

[theBait, theSwitch] = [theSwitch, theBait]
#<- meta.variable.assignment.destructured keyword.operator.assignment.destructured.array
#^^^^^^^^^^^^^^^^^^^^^ meta.variable.assignment.destructured
#^^^^^^^ variable.other.readwrite
#         ^^^^^^^^^ variable.other.readwrite
#                  ^ keyword.operator.assignment.destructured.array
#                    ^ keyword.operator.assignment

tag = "<impossible>"
[open, contents..., close] = tag.split("")
#              ^^^ keyword.operator.spread

text = "Every literary critic believes he will outwit history and have the last word"
[first, ..., last] = text.split " "
#       ^^^ keyword.operator.spread

{poet: {name, address: [street, city]}} = futurists
#<- meta.variable.assignment.destructured keyword.operator.assignment.destructured
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.variable.assignment.destructured
#^^^^ meta.mapping.key
#    ^ punctuation.separator.key-value
#      ^ keyword.operator.assignment.destructured
#       ^^^^ variable.other.readwrite
#             ^^^^^^^ meta.mapping.key
#                    ^ punctuation.separator.key-value
#                      ^ keyword.operator.assignment.destructured
#                       ^^^^^^ variable.other.readwrite
#                               ^^^^ variable.other.readwrite
#                                   ^^^ keyword.operator.assignment.destructured
#                                       ^ keyword.operator.assignment

arst = { b: 1, c: 2 }
a={b,c}=arst
#^ keyword.operator.assignment
# ^^^^^^ meta.variable.assignment.destructured
#      ^ keyword.operator.assignment
console.log a, b, c

a = (data) -> { arst, wee } = data
#             ^^^^^^^^^^^^^ meta.variable.assignment.destructured
a = (data) ->
  {
# ^ meta.variable.assignment.destructured keyword.operator.assignment.destructured
    arst
#   ^^^^ meta.variable.assignment.destructured variable.other.readwrite
    wee
#   ^^^ meta.variable.assignment.destructured variable.other.readwrite
  } = data
# ^^^ meta.variable.assignment.destructured
# ^ keyword.operator.assignment.destructured

a = {
  arst,
  wee,
}

# multi-line
{
#<- meta.variable.assignment.destructured keyword.operator.assignment.destructured
  poet: {
# ^^^^^^^ meta.variable.assignment.destructured
# ^^^^ meta.mapping.key
#     ^ punctuation.separator.key-value
#       ^ keyword.operator.assignment.destructured
    name
#   ^^^^ meta.variable.assignment.destructured variable.other.readwrite
    address: [ street, city ]
#   ^^^^^^^^^^^^^^^^^^^^^^^^^ meta.variable.assignment.destructured
#   ^^^^^^^ meta.mapping.key
#          ^ punctuation.separator.key-value
#            ^ keyword.operator.assignment.destructured
#              ^^^^^^ variable.other.readwrite
#                    ^ punctuation.separator.comma
#                      ^^^^ variable.other.readwrite
#                           ^ keyword.operator.assignment.destructured
  }
# ^ keyword.operator.assignment.destructured
} = futurists
#<- meta.variable.assignment.destructured keyword.operator.assignment.destructured
#^^ meta.variable.assignment.destructured
# ^ keyword.operator.assignment

class Person
  constructor: (options) ->
    { @name, @age, @height = 'average' } = options
#   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.variable.assignment.destructured
#   ^ keyword.operator.assignment.destructured
#     ^^^^^ variable.other.readwrite.instance
#          ^ punctuation.separator.comma
#            ^^^^ variable.other.readwrite.instance
#                ^ punctuation.separator.comma
#                  ^^^^^^^ variable.other.readwrite.instance
#                          ^ keyword.operator.assignment
#                            ^^^^^^^^^ string.quoted.single
#                                      ^ keyword.operator.assignment.destructured
#                                        ^ keyword.operator.assignment

exports.Access = class Access extends Base
  constructor: (@name, {@soak, @shorthand} = {}) ->
#                      ^ keyword.operator.assignment.destructured
#                                        ^ keyword.operator.assignment.destructured

SomeComponent = ({ something, ...props }) ->
#                ^ keyword.operator.assignment.destructured
#                                      ^ keyword.operator.assignment.destructured

SomeComponent = ({
#                ^ keyword.operator.assignment.destructured
  something
  ...props
}) ->
#<- keyword.operator.assignment.destructured
