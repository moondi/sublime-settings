# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

f = => yield this
#   ^^ invalid.illegal.bound-generator
#      ^^^^^ invalid.illegal.bound-generator

# this isn't doable unless I can figure out how to track indentation (and thus scope an entire function with meta.function)
f = =>
#   ^^ invalid.illegal.bound-generator
  doSomething()
  yield this
# ^^^^^ invalid.illegal.bound-generator

self = this
f = -> yield self
#   ^^ keyword.declaration.function
#      ^^^^^ keyword.declaration.function.generator

f = ->
#   ^^ keyword.declaration.function
  doSomething()
  yield
# ^^^^^ keyword.declaration.function.generator

b = do ->
#   ^^ keyword.declaration.function.self-invoking
#      ^^ keyword.declaration.function
  out = await a()
#       ^^^^^ keyword.declaration.function.async

result = do -> something()
#<- meta.function entity.name.function
#^^^^^^^^^^^^^ meta.function
#^^^^^ entity.name.function
#      ^ keyword.operator.assignment
#        ^^ keyword.declaration.function.self-invoking
#           ^^ keyword.declaration.function

square = (x) -> x * x
#<- meta.function entity.name.function
#^^^^^^^^ meta.function
#^^^^^ entity.name.function
#      ^ keyword.operator.assignment
#       ^ - meta.function.parameters
#        ^^^ meta.function.parameters
#        ^ punctuation.section.group.begin
#         ^ variable.parameter
#          ^ punctuation.section.group.end
#           ^^^ meta.function
#           ^^^ - meta.function.parameters
#            ^^ keyword.declaration.function

# as an object property
a = doSomething: -> console.log 'something'
#<- variable.other.readwrite
# ^ keyword.operator.assignment
#   ^^^^^^^^^^^^^^^ meta.function
#   ^^^^^^^^^^^ entity.name.function
#              ^ punctuation.separator.key-value
#                ^^ keyword.declaration.function

# with default values
fill = (container, liquid = "coffee") ->
# ^^^^^ meta.function
#      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters
#      ^ punctuation.section.group.begin
#       ^^^^^^^^^ variable.parameter
#                ^ punctuation.separator
#                  ^^^^^^ variable.parameter
#                         ^ keyword.operator.assignment
#                           ^^^^^^^^^ string.quoted.double
#                           ^ punctuation.definition.string.begin
#                                  ^ punctuation.definition.string.begin
#                                   ^ punctuation.section.group.end
#                                    ^^^ meta.function
#                                     ^^ keyword.declaration.function

# ===============
# bound functions
# ===============
$('.shopping_cart').on 'click', (event) => @customer.purchase @cart
#                   ^^ - variable.parameter
#                               ^^^^^^^ meta.function.parameters
#                               ^ punctuation.section.group.begin
#                                ^^^^^ variable.parameter
#                                     ^ punctuation.section.group.end
#                                      ^^^ meta.function
#                                       ^^ keyword.declaration.function.bound

# function argument that is an anonymous bound function
SomeApi.index some_id: 1
.done (data) => @setState data: data
.fail (response) -> GritterActions.serverError response
.always => @setState loading: false, selectedSections: [@props.selectedSection]
#^^^^^^ -variable.other.readwrite
#       ^^ keyword.declaration.function

String::dasherize = ->
#<- meta.function entity.name.function
#^^^^^^^^^^^^^^^^^^^^^ meta.function
#^^^^^^^^^^^^^^^^ entity.name.function
#     ^^ keyword.operator.prototype
#                 ^ keyword.operator.assignment
#                   ^^ keyword.declaration.function
  this.replace /_/g, "-"

Something2 = ({
#<- meta.function entity.name.function
#^^^^^^^^^^^^^^^ meta.function
#^^^^^^^^^ entity.name.function
#            ^ punctuation.section.group.begin
#             ^ meta.variable.assignment.destructured punctuation.definition.braces.begin keyword.operator
  prop1 = 'default'
# ^^^^^^^^^^^^^^^^^ meta.function meta.variable.assignment.destructured
# ^^^^^ variable.parameter
#       ^ keyword.operator.assignment
#         ^^^^^^^^^ string.quoted.single
  prop2
# ^^^^^ meta.function meta.variable.assignment.destructured variable.parameter
  isThing = true
#           ^^^^ constant.language.boolean.true
  numStuff = 4
#            ^ constant.numeric
}) ->
#<- meta.function.parameters meta.variable.assignment.destructured keyword.operator.assignment.destructured.object
#^ meta.function.parameters punctuation.section.group.end
# ^^^ meta.function
#  ^^ keyword.declaration.function
  something

grade = (student, period=(if b? then 7 else 6)) ->
#<- meta.function entity.name.function
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function
#^^^^ entity.name.function
#     ^ keyword.operator.assignment
#       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function.parameters
#        ^^^^^^^ variable.parameter
#                 ^^^^^^ variable.parameter
#                       ^ keyword.operator.assignment
  if student.excellentWork then "A+" else 'C'

BestSomething =
#<- meta.function.definition entity.name.function
#^^^^^^^^^^^^^^ meta.function.definition
#^^^^^^^^^^^^ entity.name.function
  ({
    prop1 = 'default'
    prop2
  }) ->
    <div className='text-center'>
      {prop1}
    </div>

# anonymous
-> 1
#<- keyword.declaration.function
#^ keyword.declaration.function

=>
#<- keyword.declaration.function
#^ keyword.declaration.function
  1

(b) => 1
#<- meta.function.parameters punctuation.section.group.begin
#^^ meta.function.parameters
#^ variable.parameter.function
# ^ punctuation.section.group.end
#   ^^ keyword.declaration.function.coffee

(b) ->
#<- meta.function.parameters punctuation.section.group.begin
#^^ meta.function.parameters
#^ variable.parameter.function
# ^ punctuation.section.group.end
#   ^^ keyword.declaration.function.coffee
  1

(
#<- meta.function.parameters punctuation.section.group.begin
  b
# ^ meta.function.parameters variable.parameter.function
) -> 1
#<- meta.function.parameters punctuation.section.group.end
# ^^ keyword.declaration.function.coffee

(
#<- meta.function.parameters punctuation.section.group.begin
  b
# ^ meta.function.parameters variable.parameter.function
) =>
#<- meta.function.parameters punctuation.section.group.end
# ^^ keyword.declaration.function.coffee
  1

# self-invoking
do -> 1
#<- meta.function.definition keyword.declaration.function.self-invoking
#^^^^ meta.function.definition
#^ keyword.declaration.function.self-invoking
#  ^^ keyword.declaration.function.no-arguments

do
#<- meta.function.definition keyword.declaration.function.self-invoking
#^ meta.function.definition keyword.declaration.function.self-invoking
  ->
# ^^ keyword.declaration.function.no-arguments
    1

do (a) -> 1
#<- meta.function.definition keyword.declaration.function.self-invoking
#^ meta.function.definition keyword.declaration.function.self-invoking
#  ^^^ meta.function.parameters.coffee
#     ^^^ meta.function.definition
#      ^^ keyword.declaration.function

do (
#<- meta.function.definition keyword.declaration.function.self-invoking
#^ meta.function.definition keyword.declaration.function.self-invoking
#  ^ meta.function.parameters punctuation.section.group.begin
  a
# ^ meta.function.parameters variable.parameter.function
) ->
#<- meta.function.parameters punctuation.section.group.end
# ^^ meta.function.definition keyword.declaration.function
  1

do
#<- meta.function.definition keyword.declaration.function.self-invoking
#^ meta.function.definition keyword.declaration.function.self-invoking
  (
# ^ meta.function.parameters punctuation.section.group.begin
    a
#   ^ meta.function.parameters variable.parameter.function
  ) ->
# ^ meta.function.parameters punctuation.section.group.end
#   ^^ meta.function.definition keyword.declaration.function
    1

# variable assignment
a = (b) -> 1
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^^^ meta.function.parameters
#       ^^ keyword.declaration.function
a = (b) ->
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^^^ meta.function.parameters
#       ^^ keyword.declaration.function
  1

a =
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
  (b) ->
# ^^^ meta.function.parameters
#     ^^ keyword.declaration.function
    1

a = (
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^ meta.function.parameters punctuation.section.group.begin
  b
# ^ meta.function.parameters variable.parameter.function
) ->
#<- meta.function.parameters punctuation.section.group.end
# ^^ keyword.declaration.function
  1

a =
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
  (
# ^ meta.function.parameters punctuation.section.group.begin
    b
#   ^ meta.function.parameters variable.parameter.function
  ) ->
# ^ meta.function.parameters punctuation.section.group.end
#   ^^ keyword.declaration.function
    1

# with destructuring
a = ({b}) -> 1
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^^^^^ meta.function.parameters
#   ^ punctuation.section.group.begin
#    ^^^ meta.variable.assignment.destructured
#    ^ keyword.operator.assignment.destructured.object
#     ^ variable.parameter.function
#      ^ keyword.operator.assignment.destructured.object
#         ^^ keyword.declaration.function

a = ({b}) ->
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^^^^^ meta.function.parameters
#   ^ punctuation.section.group.begin
#    ^^^ meta.variable.assignment.destructured
#    ^ keyword.operator.assignment.destructured.object
#     ^ variable.parameter.function
#      ^ keyword.operator.assignment.destructured.object
#         ^^ keyword.declaration.function
  1

a =
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
  ({b}) ->
# ^^^^^ meta.function.parameters
# ^ punctuation.section.group.begin
#  ^^^ meta.variable.assignment.destructured
#  ^ keyword.operator.assignment.destructured.object
#   ^ variable.parameter.function
#    ^ keyword.operator.assignment.destructured.object
#       ^^ keyword.declaration.function
    1

a = ({
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
#   ^^ meta.function.parameters
#   ^ punctuation.section.group.begin
#    ^ meta.variable.assignment.destructured keyword.operator.assignment.destructured.object
  b
# ^ meta.function.parameters meta.variable.assignment.destructured variable.parameter.function
}) ->
#<- meta.function.parameters meta.variable.assignment.destructured keyword.operator.assignment.destructured.object
#^ meta.function.parameters punctuation.section.group.end
#  ^^ keyword.declaration.function
  1

a =
#<- meta.function.definition entity.name.function
# ^ meta.function.definition keyword.operator.assignment
  ({
# ^^ meta.function.parameters
# ^ punctuation.section.group.begin
#  ^ meta.variable.assignment.destructured keyword.operator.assignment.destructured.object
    b
#   ^ meta.function.parameters meta.variable.assignment.destructured variable.parameter.function
  }) ->
# ^^ meta.function.parameters
# ^ meta.variable.assignment.destructured keyword.operator.assignment.destructured.object
#  ^ punctuation.section.group.end
#    ^^ keyword.declaration.function
    1
