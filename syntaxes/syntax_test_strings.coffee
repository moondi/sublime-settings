# SYNTAX TEST "Packages/User/syntaxes/Coffeescript.sublime-syntax"

'''
#<- string.quoted.single.heredoc punctuation.definition.string.begin
#^^ string.quoted.single.heredoc punctuation.definition.string.begin
'single quotes are usable without escaping'
#<- string.quoted.single.heredoc
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single.heredoc
"#{@something} <- just a regular string"
#<- string.quoted.single.heredoc
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single.heredoc
\x12 &middot; &
#<- string.quoted.single.heredoc
#^^^^^^^^^^^^^^ string.quoted.single.heredoc
#<- constant.character.escape
#^^^ constant.character.escape
#    ^^^^^^^^ constant.character.entity
#             ^ invalid.illegal.bad-ampersand
'''
#<- string.quoted.single.heredoc punctuation.definition.string.end
#^^ string.quoted.single.heredoc punctuation.definition.string.end

"""
#<- string.quoted.double.heredoc punctuation.definition.string.begin
#^^ string.quoted.double.heredoc punctuation.definition.string.begin
"double quotes are usable without escaping"
#<- string.quoted.double.heredoc
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double.heredoc
"#{something} <- interpolation"
#<- string.quoted.double.heredoc
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double.heredoc
#^^^^^^^^^^^^ source.coffee.embedded
#^^ punctuation.section.embedded.begin
#           ^ punctuation.section.embedded.end
\x12 &middot; &
#<- string.quoted.double.heredoc
#^^^^^^^^^^^^^^ string.quoted.double.heredoc
#<- constant.character.escape
#^^^ constant.character.escape
#    ^^^^^^^^ constant.character.entity
#             ^ invalid.illegal.bad-ampersand
"""
#<- string.quoted.double.heredoc punctuation.definition.string.end
#^^ string.quoted.double.heredoc punctuation.definition.string.end

'something \x12 \'escaped singles\' \"escaped doubles\" #{interpolation} &middot; &'
#<- string.quoted.single punctuation.definition.string.begin
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single
#          ^^^^ constant.character.escape
#               ^^ constant.character.escape
#                                ^^ constant.character.escape
#                                   ^^ constant.character.escape
#                                                    ^^ constant.character.escape
#                                                                        ^^^^^^^^ constant.character.entity
#                                                                                 ^ invalid.illegal.bad-ampersand
#                                                                                  ^ punctuation.definition.string.end

"something \x12 \'escaped singles\' \"escaped doubles\" #{interpolation} &middot; &"
#<- string.quoted.double punctuation.definition.string.begin
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.double
#          ^^^^ constant.character.escape
#               ^^ constant.character.escape
#                                ^^ constant.character.escape
#                                   ^^ constant.character.escape
#                                                    ^^ constant.character.escape
#                                                       ^^^^^^^^^^^^^^^^ source.coffee.embedded
#                                                       ^^ punctuation.section.embedded.begin
#                                                                      ^ punctuation.section.embedded.end
#                                                                        ^^^^^^^^ constant.character.entity
#                                                                                 ^ invalid.illegal.bad-ampersand
#                                                                                  ^ punctuation.definition.string.end

'line continuation' == 'line \
#<- string.quoted.single punctuation.definition.string.begin
#^^^^^^^^^^^^^^^^^^ string.quoted.single
#                 ^ punctuation.definition.string.end
#                      ^^^^^^ string.quoted.single
#                            ^ punctuation.separator.continuation
                        continuation'
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ string.quoted.single
